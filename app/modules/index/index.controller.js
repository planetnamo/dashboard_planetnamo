(function() {
	'use strict';
	angular
		.module('app')
		.controller('IndexController', IndexController);

	// IndexController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	IndexController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', '$timeout', 'authService', 'validationService', 'network'];

	// function IndexController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
	function IndexController($scope, $rootScope, $state, $localStorage, Facebook, $timeout, authService, validationService, network) {

		// socket.on("connect", function(){
		// 	console.log("Socket Connected!");
		// });

		$scope.razorPayKey = "rzp_test_Z8g1MeZjHFpVjA";

		// User Auth logic

		// Vars initialize for user
		$scope.user = {};
		$scope.FBUser = {};
		$scope.GoogleUser = {};
		$scope.loginCredentials = {};
		$scope.loginCredentials.email = "";
		$scope.loginCredentials.password = "";

		// Vars for global utils
		$scope.compareLots = {};
		$scope.compareLots.clearance = [];
		$scope.compareLots.auction = [];
		$scope.compareLots.buy = [];
		$scope.compareLots.rental = [];


		$scope.baseImageUrl = "http://urbanriwaaz.com:8080/api/getFile/profilePicture";

		// Check if user is already logged in and
		// get user info from localstorage
		$scope.initUserSession = function(){
			if ($localStorage.userId) {
				$scope.user = {
					"name": $localStorage.userName,
					"id": $localStorage.userId,
					"email": $localStorage.userEmail,
					"type": $localStorage.userType,
					"mobile": $localStorage.userMobile,
					"authToken": $localStorage.userAuthToken,
					"loginType": $localStorage.loginType
				};
			}
		};
		$scope.initUserSession();

		// User login with email
		$scope.loginUser = function() {
			if ($scope.loginCredentials.email && $scope.loginCredentials.email.trim != "" && validationService.isEmailValid($scope.loginCredentials.email)) {
				if ($scope.loginCredentials.password && $scope.loginCredentials.password != "") {
					// auth service for network call to login api
					$scope.loginSpinnerEnabled = true;
					authService.login($scope.loginCredentials.email, $scope.loginCredentials.password)
						.then(function(res) {
							if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
								// user found in database / login success
								$localStorage.userName = res.data.response.name;
								$localStorage.userId = res.data.response.userId;
								$localStorage.userEmail = res.data.response.email;
								$localStorage.userType = res.data.response.accountType;
								$localStorage.userMobile = res.data.response.mobile;
								$localStorage.userAuthToken = res.data.response.authToken;
								$localStorage.loginType = "email";
								swal("Welcome!", "Welcome " + res.data.response.name, "success");
								$scope.loginCredentials = {};
								$scope.initUserSession();
								$state.go("home", {}, {
									reload: true
								});
							} else if (res.data && res.data.status && res.data.status == "error") {
								swal("Not valid credentials", res.data.message, "error");
							}
							$scope.loginSpinnerEnabled = false;
						}, function(err) {
							swal("Oops...", "Something went wrong while getting logging in", "error");
							$scope.loginSpinnerEnabled = false;
						});
				} else {
					// No password Passed
					swal("Oops...", "Please enter your password", "error");
				}
			} else {
				// No email passed / Not a valid email
				swal("Oops...", "Please enter valid email address", "error");
			}
		};

		// Facebook SDK thing

		$scope.prepareFBData = function(response) {
			console.log(response);
			$scope.loginSpinnerEnabled = true;
			$scope.FBUser.loginType = "FB";
			$scope.FBUser.facebookUserId = response.id;
			$scope.FBUser.name = response.name;
			$scope.FBUser.email = response.email;
			$scope.FBUser.facebookImageUrl = response.picture.data.url;

			authService.facebookLogin($scope.FBUser)
				.then(function(res) {
					console.log(res);
					if (res.data.status == "success") {
						// $scope.showLoading = false;
						//console.log("after login:", res);
						$localStorage.userName = res.data.response.name;
						$localStorage.userId = res.data.response.userId;
						$localStorage.userEmail = res.data.response.email;
						$localStorage.userType = res.data.response.accountType;
						$localStorage.userMobile = res.data.response.userMobile;
						$localStorage.userAuthToken = res.data.response.authToken;
						$localStorage.loginType = 'FB';
						if (res.data.response.imageUrl) {
							$localStorage.imageUrl = res.data.imageUrl;
						}
						$scope.initUserSession();
						swal("Welcome " + $scope.FBUser.name, "You have successfully logged in!", "success");
						$state.go("home", {}, {
							reload: true
						});
					}
					$scope.loginSpinnerEnabled = false;
				}, function(err) {
					$scope.loginSpinnerEnabled = false;
					swal("Oops...", "Something went wrong. Please try again.", "error");
				});
		};

		$scope.me = function() {
			Facebook.api('/me', {
				fields: "name, email, gender, birthday, picture.type(large)"
			}, function(response) {
				$scope.prepareFBData(response);
			});
		};

		$scope.FBLogin = function() {
			$scope.loginSpinnerEnabled = true;
			Facebook.login(function(response) {
				// Do something with response.
				if (response.authResponse) {
					$scope.me();
					$scope.FBUser.facebookAccessToken = response.authResponse.accessToken;
				} else {
					// User denied the facebook popup
				}
			}, {
				scope: 'public_profile, email, user_birthday'
			});
		};

		$scope.FBLogout = function() {
			Facebook.logout(function(response) {});
		}

		$scope.getLoginStatus = function() {
			Facebook.getLoginStatus(function(response) {
				if (response.status === 'connected') {
					$scope.loggedIn = true;
				} else {
					$scope.loggedIn = false;
				}
			});
		};

		$scope.$watch(function() {
			return Facebook.isReady();
		}, function(newVal) {
			$scope.facebookReady = true;
			// console.log("Facebook login ready");
			// $scope.showLoading = false;
		});

		$scope.loginWithGoogle = function(){
			gapi.auth.signIn({
				clientid: "225741456791-u5tpcc3e7jj88cleukj69vflmdofjt1d.apps.googleusercontent.com",
				cookiepolicy: "single_host_origin",
				requestvisibleactions: "http://schemas.google.com/AddActivity",
				scope: "https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read",
				callback: function(authResult) {
					if(authResult.id_token){
						$scope.GoogleUser.googleAccessToken = authResult.id_token;
						$scope.signInCallback(authResult);
					}
				}

			}); 
		};

		$scope.signInCallback = function(authResult) {
			$scope.getUserInfo();
			$scope.loginSpinnerEnabled = true;
			console.log(authResult);
		};

		$scope.getUserInfo = function() {
			gapi.client.request(
			{
				'path':'/plus/v1/people/me',
				'method':'GET',
				'callback': function(userInfo){
					$scope.prepareGoogleData(userInfo);
				}
			}
			);
		};

		$scope.prepareGoogleData = function(response){
			if(response.emails && response.emails.length>0 && response.emails[0].value){
				$scope.GoogleUser.loginType = "GOOGLE";
				$scope.GoogleUser.googleUserId = response.id;
				$scope.GoogleUser.name = response.displayName;
				$scope.GoogleUser.email = response.emails[0].value;
				$scope.GoogleUser.gender = response.gender;

				authService.googleLogin($scope.GoogleUser)
				.then(function(response){

					if(response.data.status == "success"){
						$localStorage.userName = response.data.response.name;
						$localStorage.userId = response.data.response.userId;
						$localStorage.userEmail = response.data.response.email;
						$localStorage.userType = response.data.response.accountType;
						$localStorage.userMobile = response.data.response.userMobile;
						$localStorage.userAuthToken = response.data.response.authToken;
						$localStorage.loginType = 'FB';

						$scope.loginSpinnerEnabled = false;
						$scope.initUserSession();
						swal("Welcome " + $scope.GoogleUser.name, "You have successfully logged in!", "success");
						$state.go("home", {}, {
							reload: true
						});
					}else {
						$scope.loginSpinnerEnabled = false;
						swal("Oops...", "Something went wrong. Please try again.", "error");
					}
				}, function(err){
					$scope.loginSpinnerEnabled = false;
					swal("Oops...", "Something went wrong. Please try again.", "error");
				});
			}else{
				$scope.loginSpinnerEnabled = false;
				swal("Error", "Unable to get email. Please try again.", "error");
			}
		};

		// Comments:
		// Login user code is written in login-modal.controller

		if (authService.isUserLoggedIn()) {
			// console.log("User logged in");
		} else {
			// console.log("User not logged in");
		}

		$scope.logoutUser = function() {
			$timeout(function() {
				$scope.user = {};
				$localStorage.$reset();
				if ($localStorage.loginType == 'FB') {
					$scope.FBLogout();
				}
				console.log("working");
				$state.go('login', {}, {
					reload: true
				});
			});
		};

		// Forgot password
		$scope.forgotPassword = function() {
			swal({
					title: "Reset Password",
					text: "Please enter your registered Email Id",
					type: "input",
					showCancelButton: true,
					closeOnConfirm: false,
					animation: "slide-from-top",
					inputPlaceholder: "Email Id"
				},
				function(inputValue) {
					if (inputValue === false) return false;

					if (inputValue.trim() === "") {
						swal.showInputError("Email address can not be blank!");
						return false
					}

					network.forgotPassword(inputValue)
					.then(function(res){
						if(res.data && res.data.status.toLowerCase() == 'success'){
							swal("Almost Done!", "An email with password reset instructions has been sent to " + inputValue, "success");
						} else if(res.data && res.data.status.toLowerCase() == 'error'){
							swal("Oops...", res.data.message, "error");
						} else {
							swal("Oops...", "Something went wrong. Please try again", "error");
						}
					}, function(err){
						swal("Oops...", "Unable to reach servers. Please try after sometime.", "error");
					});
				});
		};

		// Redirect user to home if not logged in
		$scope.userAuthRequired = function() {
			if (!$scope.user.id) {
				$state.go("home", {}, {
					reload: true
				});
			}
		};

		$scope.showLoginDialog = function(){
			$('#login-signup-modal').modal('show');
		}

		$scope.animateToTop = function(className){
			$('html, body').animate({
				scrollTop: $('.'+className).offset().top
			}, 'normal');
		};


	};

})();