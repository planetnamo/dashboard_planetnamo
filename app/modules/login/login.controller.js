(function() {
	'use strict';
	angular
	.module('app')
	.controller('LoginController', LoginController);

	// LoginController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	LoginController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', '$stateParams', '$timeout', 'validationService', 'authService', 'network'];

	// function LoginController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function LoginController($scope, $rootScope, $state, $localStorage, $stateParams, $timeout, validationService, authService, network) {
			
			$scope.loginCredentials = {};

			// User login with email
			$scope.login = function() {
				if ($scope.loginCredentials.email && $scope.loginCredentials.email.trim != "" && validationService.isEmailValid($scope.loginCredentials.email)) {
					if ($scope.loginCredentials.password && $scope.loginCredentials.password != "") {
						// auth service for network call to login api
						$scope.loginSpinnerEnabled = true;
						authService.login($scope.loginCredentials.email, $scope.loginCredentials.password)
							.then(function(res) {
								if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
									// user found in database / login success
									if(res.data.response.accountType == 1){
										swal("Unauthorized Attempt", "It looks like you are an Individual User. Please go to the login section on planetnamo.com and try again.", "error");
										$scope.loginCredentials = {};
									} else {
										$localStorage.userName = res.data.response.name;
										$localStorage.userId = res.data.response.userId;
										$localStorage.userEmail = res.data.response.email;
										$localStorage.userType = res.data.response.accountType;
										$localStorage.userMobile = res.data.response.mobile;
										$localStorage.userAuthToken = res.data.response.authToken;
										$localStorage.loginType = "email";
										swal("Welcome!", "Welcome " + res.data.response.name, "success");
										$scope.loginCredentials = {};
										$scope.initUserSession();
										console.log("in login");
										$state.go("dashboard.auctions", {}, {
											reload: true
										});
									}
								} else if (res.data && res.data.status && res.data.status == "error") {
									swal("Not valid credentials", res.data.message, "error");
								}
								$scope.loginSpinnerEnabled = false;
							}, function(err) {
								swal("Oops...", "Something went wrong while getting logging in", "error");
								$scope.loginSpinnerEnabled = false;
							});
							// $localStorage.userName = "dev";
							// $localStorage.userId = "123456";
							// $localStorage.userEmail = "kd@gmail.com";
							// $localStorage.userType = "Admin";
							// $localStorage.userMobile = "9999999999";
							// $localStorage.userAuthToken = "authToken";
							// $localStorage.loginType = "email";
							// swal("Welcome!", "Welcome " + "Dev", "success");
							// $scope.loginCredentials = {};
							// $scope.initUserSession();
							// console.log("in login");
							// $state.go("dashboard.buy", {}, {
							// 	reload: true
							// });
					} else {
						// No password Passed
						swal("Oops...", "Please enter your password", "error");
					}
				} else {
					// No email passed / Not a valid email
					swal("Oops...", "Please enter valid email address", "error");
				}
			};

		};

	})();