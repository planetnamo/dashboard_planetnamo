(function() {
	'use strict';
	angular
		.module('app')
		.factory('network', network);

	network.$inject = ['$http'];

	function network($http) {

		// var baseUrl = "http://urbanriwaaz.com:8080";
		var baseUrl = "http://54.200.74.162:3000";
		// var baseUrl = "http://localhost:3000";
		//var baseUrl = "http://planetnamo.com:3000";
		// var baseUrl = "http://54.149.66.54:8080";
		var network = {};

		// var authToken= "1ac599298804d6750e5ae1a72b83737d31a41277";

		// user profile apis
		network.getUserInfo = function(authToken) {
			return $http({
				method: "GET",
				url: baseUrl + '/api/user/me',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				}
			});
		};

		network.editUserInfo = function(authToken, data) {
			return $http({
				method: "PUT",
				url: baseUrl + '/api/user/editUser',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: data
			});
		};

		network.uploadProfilePicture = function(authToken, picture) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/user/uploadPicture',
				headers: {
					'Content-Type': undefined,
					'Authorization': 'JWT ' + authToken
				},
				data: picture
			});
		};

		network.addAddress = function(authToken, address) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/user/addAddress',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: address
			});
		};

		network.editAddress = function(authToken, address, addressId) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/user/editAddress/'+ addressId,
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: address
			});
		};

		network.changePassword = function(authToken, data) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/user/ChangePassword',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: data
			});
		};

		network.forgotPassword = function(email) {
			return $http({
				method: "GET",
				url: baseUrl + '/api/auth/forgotPassword/'+email,
				headers: {
					'Content-Type': 'application/json'
				}
			});
		};

		network.resetPassword = function(email, token, data) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/auth/forgotPassword/reset/'+email+'/'+ token,
				headers: {
					'Content-Type': 'application/json'
				},
				data: data
			});
		};

		network.getProductCategories = function(data) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/product/GetProductCategories',
				headers: {
					'Content-Type': 'application/json'
				},
				data: data
			});
		};

		network.getProductsInCategory = function(categoryId) {
			return $http({
				method: "GET",
				url: baseUrl + '/api/product/GetBaseProductsBelongingToCategory/' + categoryId,
				headers: {
					'Content-Type': 'application/json'
				}
			});
		};

		network.getSellingProductSpecs = function(productId) {
			return $http({
				method: "GET",
				url: baseUrl + '/api/product/GetSpecificBaseproduct/' + productId,
				headers: {
					'Content-Type': 'application/json'
				}
			});
		};

		network.createNewProduct = function(authToken, data) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/product/CreateNewProduct',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: data
			});
		};

		network.createSellingOrder = function(authToken, data) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/sell/createSellingOrder',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: data
			});
		}

		// Favourites / Wishlist apis
		network.getWishlist = function(authToken) {
			return $http({
				method: "GET",
				url: baseUrl + '/api/user/getWishlist',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				}
			});
		};

		network.addToWishlist = function(authToken, lotId) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/user/addToWishlist/' + lotId,
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				}
			});
		};

		network.removeFromWishlist = function(authToken, lotId) {
			return $http({
				method: "DELETE",
				url: baseUrl + '/api/user/removeFromWishlist/' + lotId,
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				}
			});
		};

		// Lots related apis
		network.getLots = function(authToken, filters) {
			if (arguments[0]) {
				var headers = {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				};
			} else {
				var headers = {
					'Content-Type': 'application/json'
				};
			}
			if (arguments[1]) {
				var data = filters;
			} else {
				var data = null;
			}
			return $http({
				method: "POST",
				url: baseUrl + '/api/lots/getLots',
				headers: headers,
				data: {
					filters: filters
				}
			});
		};

		// Lots related apis
		network.getLotDetail = function(authToken, lotId) {
			if (arguments[0]) {
				var headers = {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				};
			} else {
				var headers = {
					'Content-Type': 'application/json'
				};
			}
			return $http({
				method: "GET",
				url: baseUrl + '/api/lots/getLotDetail/' + lotId,
				headers: headers,
			});
		};

		// Lots related apis
		network.getHotSellingProducts = function(authToken, filters) {
			if (arguments[0]) {
				var headers = {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				};
			} else {
				var headers = {
					'Content-Type': 'application/json'
				};
			}
			return $http({
				method: "POST",
				url: baseUrl + '/api/lots/GetHotSellingLots',
				headers: headers,
				data: {
					filters: filters
				}
			});
		};

		// Lots related apis
		network.getSimilarLots = function(authToken, lotId, filters) {
			if (arguments[0]) {
				var headers = {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				};
			} else {
				var headers = {
					'Content-Type': 'application/json'
				};
			}
			return $http({
				method: "POST",
				url: baseUrl + '/api/lots/FindSimilarLots/' + lotId,
				headers: headers,
				data: {
					filters: filters
				}
			});
		};

		//PROFILE
		//WALLET
		network.getWalletAmountOfUser = function(authToken) {
			return $http({
				method: "GET",
				url: baseUrl + '/api/wallet/getWalletAmount',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				}
			});
		}

		network.getUserBankDetails = function(authToken) {
			return $http({
				method: "GET",
				url: baseUrl + '/api/user/GetUserBankData',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				}
			});
		}

		network.addAmountToWallet = function(authToken, apiData) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/wallet/addAmountToWalletWithNewRazorPayId',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: apiData
			});
		}

		network.updateWithdrawlAccountDetails = function(authToken, apiData) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/user/AddUserBankData',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: apiData
			});
		}

		network.requestForAmountWithdraw = function(authToken, apiData) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/wallet/withDrawAmountFromWallet',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: apiData
			});
		}

		network.getUserWalletTransactionHistory = function(authToken) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/wallet/getWalletTransactions',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: {}
			});
		}

		// Direct buy
		network.placeDirectBuyOrder = function(authToken, postData) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/buy/PlaceDirectBuyOrder/',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: postData
			});
		}

		// Place rental order
		network.placeRentalOrder = function(authToken, postData) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/rental/PlaceRentalOrder/',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: postData
			});
		}

		// auction
		network.payEMDForAuction = function(authToken, postData) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/auction/PayEMDOnAuction/',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: postData
			});
		}

		//CLEARANCE BUY
		network.payDepositForClearance = function(authToken, postData) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/buy/PayAggregateOrderDeposit/',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: postData
			});
		}

		//ORDER HISTORY
		network.getOrderHistoryOfUser = function(authToken, postData) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/buy/GetDirectBuyingOrdersOfUser',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: postData
			});
		}

		//AGGREGATE ORDERS
		network.getAggregateOrdersOfUser = function(authToken, postData) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/buy/GetAggregateBuyingOrdersOfUser',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: postData
			});
		}

		//BackOut FROM AGGREGATE ORDER
		network.backOutFromAggregateOrder = function(authToken, postData) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/buy/WithdrawFromAggregateBuying',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: postData
			});
		}

		//BUY AGGREGATE ORDER
		network.buyAggregateOrder = function(authToken, postData) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/buy/BuyAggregateOrder',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: postData
			});
		}

		//MY SALES
		network.getSalesOfUser = function(authToken, postData) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/sell/GetUserSellingOrders',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: postData
			});
		}

		//CANCEL SELLING ORDER
		network.cancelSellingOrderByUser = function(authToken, sellingOrderId) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/sell/CancelSellingOrder/' + sellingOrderId,
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: {}
			});
		}

		//CANCEL SELLING ORDER
		network.acceptSellingOrderByUser = function(authToken, sellingOrderId) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/sell/AcceptPriceAfterPhysicalCheck/' + sellingOrderId,
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: {}
			});
		}

		//REQUST CHANGE IN PICKUP SELLING ORDER
		network.requestChangeDateOfSellOrder = function(authToken, postData) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/delivery/RequestChangeInPickUpDate',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: postData
			});
		}


		//MY RENTALS
		network.getRentalsOfUser = function(authToken, postData) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/rental/MyRentals/',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: postData
			});
		};

		network.returnRentalOrder = function(authToken, postData) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/rental/ReturnRentalOrder/',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: postData
			});
		}

		//USER AUCTIONS
			//LIVE AUCTIONS
			network.getLiveAuctionsOfUser = function(authToken, postData) {
				return $http({
					method: "POST",
					url: baseUrl + '/api/auction/UserAuctions',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': 'JWT ' + authToken
					},
					data: postData
				});
			}

			//PLACE BID IN AUCTION
			network.placeBidInAuction = function(authToken, postData) {
				return $http({
					method: "POST",
					url: baseUrl + '/api/bid/PlaceBid',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': 'JWT ' + authToken
					},
					data: postData
				});
			}

			//EXPIRED AUCTIONS
			network.getExpiredAuctionsOfUser = function(authToken, postData) {
				return $http({
					method: "POST",
					url: baseUrl + '/api/auction/UserAuctions',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': 'JWT ' + authToken
					},
					data: postData
				});
			}

			//WON AUCTIONS
			network.getWonAuctionsOfUser = function(authToken, postData) {
				return $http({
					method: "POST",
					url: baseUrl + '/api/auction/UserAuctions',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': 'JWT ' + authToken
					},
					data: postData
				});
			}

			//BUY WON AUCTION
			network.buyWonAuctionOfUser = function(authToken, postData) {
				return $http({
					method: "POST",
					url: baseUrl + '/api/bid/payRemainingDeposit',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': 'JWT ' + authToken
					},
					data: postData
				});
			}

			//BACK OUT FROM WON AUCTION
			network.backOutFromWonAuctionOfUser = function(authToken, postData) {
				return $http({
					method: "POST",
					url: baseUrl + '/api/bid/cancelBid',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': 'JWT ' + authToken
					},
					data: postData
				});
			}

		// misc apis
		network.sendEmail = function(data) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/misc/SendEmail',
				headers: {
					'Content-Type': 'application/json'
				},
				data: data
			});
		};

		network.removeAddress = function(authToken, addressId) {
			return $http({
				method: "DELETE",
				url: baseUrl + '/api/user/deleteAddress/'+ addressId,
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				}
			});
		};

		network.compareLots = function(data) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/lots/CompareLots',
				headers: {
					'Content-Type': 'application/json'
				},
				data:{
					"lotIdArray": data
				}
			});
		};

		network.doesLotExist = function(filters) {
			return $http({
				method: "POST",
				url: baseUrl + '/api/lots/doesLotExist',
				headers: {
					'Content-Type': 'application/json'
				},
				data:{
					filters: filters,
					limit: 1
				}
			});
		};

		network.getAllUsers = function(authToken){
			return $http({
				method: "POST",
				url: baseUrl + '/api/user/GetAllUsers',
				headers:{
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				}
			});
		};

		network.getAllBaseProducts = function(){
			return $http({
				method: "POST",
				url: baseUrl + '/api/product/GetBaseProducts',
				headers:{
					'Content-Type': 'application/json'
				}
			});
		};

		network.createFinalBuyingLotFromAdminPanel = function(authToken, postData){
			return $http({
				method: "POST",
				url: baseUrl + '/api/lots/createFinalBuyingLotFromAdminPanel',
				headers:{
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
					data: postData
			});
		};

		network.getUserSellingOrders = function(authToken){
			return $http({
				method: "POST",
				url: baseUrl + '/api/sell/GetUserSellingOrders',
				headers:{
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				}
			});
		};

		network.editUserByAdmin = function(authToken, data){
			return $http({
				method: "POST",
				url: baseUrl + '/api/user/EditUserBySuperAdmin',
				headers:{
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: data
			});
		};

		network.deleteUserByAdmin = function(authToken, userId){
			return $http({
				method: "DELETE",
				url: baseUrl + '/api/user/deleteUser/'+ userId,
				headers:{
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				}
			});
		};

		network.getBaseProductInfo = function(baseProductId){
			return $http({
				method: "GET",
				url: baseUrl + '/api/product/GetSpecificBaseproduct/' + baseProductId,
				headers:{
					'Content-Type': 'application/json'
				}
			});
		};

		network.editBaseProduct = function(authToken, data){
			return $http({
				method: "PUT",
				url: baseUrl + '/api/product/EditBaseProduct',
				headers:{
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
				data: data
			});
		};

		network.createNewBaseProductAdminPanel = function(authToken, postData){
			return $http({
				method: "POST",
				url: baseUrl + '/api/product/AddBaseProductAdminPanel',
				headers:{
					'Content-Type': 'application/json',
					'Authorization': 'JWT ' + authToken
				},
					data: postData
			});
		};

		return network;

	};
})();