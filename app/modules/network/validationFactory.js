(function() {
	'use strict';
	angular
	.module('app')
	.factory('validationService', validationService);

	validationService.$inject = [];

	function validationService() {

		var validationService = {};


		validationService.isEmailValid = function(email) {
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		};

		validationService.isPhoneValid = function(phone){
			var re = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/;
			return re.test(phone);
		};

		return validationService;

	};
})();