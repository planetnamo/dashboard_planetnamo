(function() {
	'use strict';
	angular
	.module('app')
	.factory('authService', authService);

	authService.$inject = ['$state', '$http', '$localStorage'];

	function authService($state, $http, $localStorage) {

		var baseUrl = "http://54.200.74.162:3000";
		// var baseUrl = "http://localhost:3000";
		// var baseUrl = "http://urbanriwaaz.com:8080";
		//var baseUrl = "http://planetnamo.com:3000";
		var authService = {};

		// Email Login
		authService.login = function(userEmail, password) {
			return $http({
				method: 'POST',
				url: baseUrl + '/api/auth/login',
				headers: {
					'Content-Type': 'application/json'
				},
				data: {"email": userEmail, "password": password}
			});
		};

		// Facebook Login
		authService.facebookLogin = function(data) {
			return $http({
				method: 'POST',
				url: baseUrl + '/api/auth/login',
				headers: {
					'Content-Type': 'application/json'
				},
				data: data
			});
		};

		// Facebook Login
		authService.googleLogin = function(data) {
			return $http({
				method: 'POST',
				url: baseUrl + '/api/auth/login',
				headers: {
					'Content-Type': 'application/json'
				},
				data: data
			});
		};

		// Check if user is logged in
		authService.isUserLoggedIn = function() {
			if ($localStorage.userId) {
				return true;
			} else {
				return false;
			}
		};

		// Get user type
		// 1 for Individual and 2 for Business
		authService.getActiveUserType = function() {
			if (authService.isUserLoggedIn()) {
				return $localStorage.userType;
			} else {
				return false;
			}
		};


		authService.signup = function(data) {
			return $http({
				method: 'POST',
				url: baseUrl + '/api/auth/signup',
				headers: {
					'Content-Type': 'application/json'
				},
				data: data
			});
		};

		return authService;

	};
})();