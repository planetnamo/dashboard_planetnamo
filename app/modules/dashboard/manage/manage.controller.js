(function() {
	'use strict';
	angular
	.module('app')
	.controller('ManageController', ManageController);

	ManageController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'authService', 'validationService', 'network'];

	function ManageController($scope, $rootScope, $state, $timeout, authService, validationService, network) {

		$scope.userType = [];
		$scope.userType[1] = "Individual";
		$scope.userType[2] = "Business";
		$scope.userType[3] = "Super admin";

		$scope.showPasswordForm = false;
		$scope.userChagnePassword = {};

		$scope.userEditInfo = {};

		$scope.addedSpecifications = [];
		$scope.newSpecification = {};
		$scope.addedProblems = [];
		$scope.newProblem = {};
		$scope.addedAccessories = [];
		$scope.newAccessory = {};


		$scope.newSpecificationNew = {};
		$scope.newProblemNew = {};
		$scope.newAccessoryNew = {};

		$scope.selectedScreen = [];

		$scope.clearAndGenerateNewBaseProduct = function(){
			$scope.newBaseProduct = {};
			$scope.newBaseProduct.accessoriesList = [];
			$scope.newBaseProduct.problemsList = [];
			$scope.newBaseProduct.specifications = [];
		}

		$scope.clearAndGenerateNewBaseProduct();

		// for date picker on the edit product screen
		$scope.today = function() {
			$scope.dt = new Date();
			$scope.dtEnd = new Date();
		};
		$scope.today();

		$scope.clear = function() {
			$scope.dt = null;
		};

		$scope.dateOptions = {
			formatYear: 'yy',
			showWeeks: false,
			// maxDate: new Date(2020, 1, 1),
			// minDate: new Date(),
			startingDay: 1
		};

		$scope.open1 = function() {
			$scope.popup1.opened = true;
		};

		$scope.popup1 = {
			opened: false
		};

		$scope.open2 = function() {
			$scope.popup2.opened = true;
		};

		$scope.popup2 = {
			opened: false
		};

		$scope.format = "yyyy-MM-dd";
		// date picker stuff ends

		$scope.goTo = function(screen){
			if(screen == 'users' || screen == 'products' || screen == 'sellRequests'){
				$scope.selectedScreen = [];
				$scope.selectedScreen[screen] = "btn-green";
				$scope.activeScreen = screen;

				if(screen == 'users'){
					$scope.getUsers();
				} else if(screen == 'products'){
					$scope.getBaseProducts();
				} else {
					$scope.getSellRequests();
				}
			}
		};

		$scope.showChangePassword = function(){
			$scope.showPasswordForm = !$scope.showPasswordForm;
		};

		$scope.clearPasswordForm = function(){
			$scope.userChagnePassword = {};
			$scope.showPasswordForm = false;
		};

		$scope.goToSelectCategory = function(){
			$scope.showAddProductForm = false;
			$scope.showAddCategoryForm = true;
		};

		$scope.showAddNewProduct = function(){
			$scope.showAddCategoryForm = !$scope.showAddCategoryForm;
			$scope.showAddProductForm = false;
			$scope.clearAndGenerateNewBaseProduct();
		};

		$scope.selectCategory = function(category){
			$scope.selectedCategory = category;
			$scope.loadingOnCategoryId = true;

			var prepareData = {};
			prepareData.filters = {};
			prepareData.filters.queryParam = category;
			network.getProductCategories(prepareData)
			.then(function(res){
				if(res.data.status && res.data.status.toLowerCase() == 'success'){
					$scope.selectedCategoryId = res.data.response[0]._id;
					$scope.newBaseProduct.productCategoryId = res.data.response[0]._id;
					$scope.showAddProductForm = true;
					$scope.showAddCategoryForm = false;
				} else if(res.data.status && res.data.status.toLowerCase() == 'error'){
					swal('Oops...', res.data.message, "error");
				}
				$scope.loadingOnCategoryId = false;
			}, function(err){
				console.log(err);
				$scope.loadingOnCategoryId = false;
			});
		};

		$scope.addProduct = function(){
			$scope.loadingOnCategoryId = true;
			network.createNewBaseProductAdminPanel($scope.user.authToken, {'baseProductInfo': $scope.newBaseProduct})
						.then(function(res){
							console.log(res);
							if(res.data && res.data.status && res.data.status == 'success'){
								swal("Success", "Upload Successful.", "success");
								$scope.showAddNewProduct();
							}else if(res.data && res.data.message){
								swal("Oops...", res.data.message, "error");
							}else{
								swal("Oops...", "Something went wrong. Please try again.", "error");
							}
							$scope.loadingOnCategoryId = false;
						}, function(err){
							$scope.loadingOnCategoryId = false;
							console.log(err);
							swal("Oops...", "Something went wrong. Please try again.", "error");
						});
		};

		$scope.getUsers = function(){
			$scope.loadingUsers = true;
			network.getAllUsers($scope.user.authToken)
			.then(function(res){
				if(res.data && res.data.status && res.data.status.toLowerCase() == "success"){
					$scope.users = res.data.users;
					console.log($scope.users);
				} else if(res.data && res.data.status && res.data.status.toLowerCase() == "error"){
					swal("Oops...", res.data.message, "error");
				}
				$scope.loadingUsers = false;
			}, function(err){
				console.log(err);
				$scope.loadingUsers = false;
				swal("Oops...", "Something went wrong while getting logging in", "error");
			});
		};

		// $scope.goTo('users');

		$scope.getBaseProducts = function(){
			// $scope.loadingBaseProducts = true;
			// network.getAllBaseProducts()
			// .then(function(res){
			// 	if(res.data && res.data.status && res.data.status.toLowerCase() == "success"){
			// 		$scope.baseProducts = res.data.response;
			// 	} else if(res.data && res.data.status && res.data.status.toLowerCase() == "error"){
			// 		swal("Oops...", res.data.message, "error");
			// 	}
			// 	$scope.loadingBaseProducts = false;
			// }, function(err){
			// 	console.log(err);
			// 	$scope.loadingBaseProducts = false;
			// 	swal("Oops...", "Something went wrong while getting logging in", "error");
			// });
		};

		$scope.goTo('products');

		$scope.getSellRequests = function(){
			$scope.loadingSellRequests = true;
			network.getUserSellingOrders($scope.user.authToken)
			.then(function(res){
				if(res.data && res.data.status && res.data.status.toLowerCase() == "success"){
					$scope.userSellingOrders = res.data.response;
				} else if(res.data && res.data.status && res.data.status.toLowerCase() == "error"){
					swal("Oops...", res.data.message, "error");
				}
				$scope.loadingSellRequests = false;
			}, function(err){
				console.log(err);
				$scope.loadingSellRequests = false;
				swal("Oops...", "Something went wrong while getting logging in", "error");
			});
		};

		$scope.viewUserInfo = function(user){
			$scope.viewUserData = user;
			$('#viewUserInfoModal').modal('show');
		};

		$scope.editUserByAdmin = function(){
			var prepareData = {};
			prepareData.userId = $scope.userEditInfo.userId;

			if($scope.userEditInfo.name && $scope.userEditInfo.name.trim() != ""){
				prepareData.name = $scope.userEditInfo.name;
			} else {
				swal("Oops...", "Please enter user name", "error");
				return  false;
			}
			if($scope.userEditInfo.password && $scope.userEditInfo.password.trim() != ""){
				prepareData.password = $scope.userEditInfo.password;
			} else {
				// swal("Oops...", "Please enter user password", "error");
				// return  false;
			}
			if($scope.userEditInfo.mobileNumber && $scope.userEditInfo.mobileNumber.trim() != ""){
				if(validationService.isPhoneValid($scope.userEditInfo.mobileNumber)){
					prepareData.mobileNumber = $scope.userEditInfo.mobileNumber;
				} else {
					swal("Oops...", "Please enter a valid mobile number.", "error");
					return false;
				}
			} else {
				// swal("Oops...", "Please enter your mobile number.", "error");
				// return false;
			}
			if($scope.userEditInfo.email && $scope.userEditInfo.email.trim() != ""){
				if(validationService.isEmailValid($scope.userEditInfo.email)){
					prepareData.email = $scope.userEditInfo.email;
				} else {
					swal("Oops...", "Please enter a valid email id.", "error");
					return false;
				}
			} else {
				swal("Oops...", "Please enter user email id.", "error");
				return false;
			}

			$scope.loadingOnEditUser = true;
			network.editUserByAdmin($scope.user.authToken, prepareData)
			.then(function(res){
				console.log(res);
				if(res.data.status && res.data.status.toLowerCase() == "success"){
					$scope.userEditInfo = {};
					$('#editUserModal').modal('hide');
					swal("Updated", "User info has been updated successfully.", "success");
					$scope.goTo('users');
				} else if(res.data.status && res.data.status.toLowerCase() == "error"){
					swal("Oops...", res.data.message, "error");
				}
				$scope.loadingOnEditUser = false;
			}, function(err){
				console.log(err);
				$scope.loadingOnEditUser = false;
				swal("Oops...", "Something went wrong. Please try again later", "error");
			});
		};

		$scope.editUser = function(user){
			console.log(user);
			$scope.userEditInfo.userId = user._id;
			$scope.userEditInfo.name = user.name;
			$scope.userEditInfo.email = user.email;
			if(user.mobileNumber){
				$scope.userEditInfo.mobileNumber = user.mobileNumber;
			}
			console.log($scope.userEditInfo);
			$('#editUserModal').modal('show');
		};

		$scope.deleteUser = function(user){
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this user("+ user.name +"). Data will be deleted permanently.",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#3aa246",
				confirmButtonText: "Yes, delete!",
				closeOnConfirm: false
			},
			function(){
				network.deleteUserByAdmin($scope.user.authToken, user._id)
				.then(function(res){
					if(res.data.status && res.data.status.toLowerCase() == "success"){
						swal("Deleted!", "User has been deleted from the database successfully.", "success");
						$scope.goTo('users');
					} else if (res.data.status && res.data.status.toLowerCase() == "error"){
						swal("Oops...", res.data.message, "error");
					}
				},function(err){
					console.log(err);
					swal("Oops...", "Something went wrong. Please try again.", "error");
				});
			});
		};

		$scope.viewProductInfo = function(product){
			console.clear();
			console.log(product);
			$('#viewProductInfoModal').modal('show');

			$scope.loadingOnViewProductInfo = true;
			network.getBaseProductInfo(product._id)
			.then(function(res){
				console.log(res);
				if(res.data.status && res.data.status.toLowerCase() == "success"){
					$scope.productInfo = res.data.response;
				} else if (res.data.status && res.data.status.toLowerCase() == "error"){
					swal("Oops...", res.data.message, "error");
				}
				$scope.loadingOnViewProductInfo = false;
			},function(err){
				console.log(err);
				swal("Oops...", "Something went wrong. Please try again.", "error");
				$scope.loadingOnViewProductInfo = false;
			});
		};

		$scope.editProduct = function(product){
			console.clear();
			console.log(product);
			$('#editProductModal').modal('show');
			$scope.addedSpecifications = {};
			$scope.addedProblems = {};
			$scope.addedAccessories = {};
			$scope.editBaseProduct = {};

			$scope.loadingOnEditBaseProduct = true;
			network.getBaseProductInfo(product._id)
			.then(function(res){
				console.log(res);
				if(res.data.status && res.data.status.toLowerCase() == "success"){
					$scope.editBaseProduct = res.data.response;
					$scope.dt = new Date($scope.editBaseProduct.releaseDate);
					$scope.addedSpecifications = angular.copy($scope.editBaseProduct.specifications);
					$scope.addedProblems = angular.copy($scope.editBaseProduct.problemsListList);
					$scope.addedAccessories = angular.copy($scope.editBaseProduct.accessoriesListList);
				} else if (res.data.status && res.data.status.toLowerCase() == "error"){
					swal("Oops...", res.data.message, "error");
				}
				$scope.loadingOnEditBaseProduct = false;
			},function(err){
				console.log(err);
				swal("Oops...", "Something went wrong. Please try again.", "error");
				$scope.loadingOnEditBaseProduct = false;
			});
		};

		$scope.addNewSpecification = function(){
			if($scope.newSpecification.displayName && $scope.newSpecification.displayName.trim() != ""){
				if($scope.newSpecification.specificationValue && $scope.newSpecification.specificationValue.trim() != ""){
					$scope.addedSpecifications.push($scope.newSpecification);
					$scope.newSpecification = {};
				} else {
					swal("Oops...", "Specification value is required.", "error");
				}	
			} else {
				swal("Oops...", "Specification name is required.", "error");
			}
		};

		$scope.removeSpec = function(index){
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this data.",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#3aa246",
				confirmButtonText: "Yes, Remove!",
				closeOnConfirm: true
			},
			function(){
				console.log($scope.addedSpecifications);
				console.log("length", $scope.addedSpecifications.length);

				$scope.addedSpecifications.splice(index, 1);

				console.log($scope.addedSpecifications);
				console.log("length", $scope.addedSpecifications.length);
				$scope.$apply();
			});
		};

		$scope.addNewProblem = function(){
			if($scope.newProblem.problemname && $scope.newProblem.problemname.trim() != ""){
				if($scope.newProblem.remarks && $scope.newProblem.remarks.trim() != ""){
					$scope.addedProblems.push($scope.newProblem);
					$scope.newProblem = {};
				} else {
					swal("Oops...", "Problem remarks is required.", "error");
				}	
			} else {
				swal("Oops...", "Problem name is required.", "error");
			}
		};
		$scope.removeProblem = function(index){
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this data.",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#3aa246",
				confirmButtonText: "Yes, Remove!",
				closeOnConfirm: true
			},
			function(){
				$scope.addedProblems.splice(index, 1);
				$scope.$apply();
			});
		};

		$scope.addNewAccessory = function(){
			if($scope.newAccessory.accessoryname && $scope.newAccessory.accessoryname.trim() != ""){
				$scope.addedAccessories.push($scope.newAccessory);
				$scope.newAccessory = {};	
			} else {
				swal("Oops...", "Accessory name is required.", "error");
			}
		};
		$scope.removeAccessory = function(index){
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this data.",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#3aa246",
				confirmButtonText: "Yes, Remove!",
				closeOnConfirm: true
			},
			function(){
				$scope.addedAccessories.splice(index, 1);
				$scope.$apply();
			});
		};

		$scope.editProductByAdmin = function(){
			console.log($scope.editBaseProduct);
			$scope.editBaseProduct.specifications = $scope.addedSpecifications;
			$scope.editBaseProduct.problemsListList = $scope.addedProblems;
			$scope.editBaseProduct.accessoriesListList = $scope.addedAccessories;

			network.editBaseProduct($scope.user.authToken, $scope.editBaseProduct)
			.then(function(res){
				console.log(res);
			}, function(err){
				console.log(err);
			});
		};


		//ADD NEW BASE PRODUCT
		$scope.addNewSpecificationNew = function(){
			if($scope.newSpecificationNew.displayName && $scope.newSpecificationNew.displayName.trim() != ""){
				if($scope.newSpecificationNew.specificationValue && $scope.newSpecificationNew.specificationValue.trim() != ""){
					$scope.newBaseProduct.specifications.push($scope.newSpecificationNew);
					$scope.newSpecificationNew = {};
				} else {
					swal("Oops...", "Specification value is required.", "error");
				}	
			} else {
				swal("Oops...", "Specification name is required.", "error");
			}
		};

		$scope.removeSpecNew = function(index){
			$scope.newBaseProduct.specifications.splice(index, 1);
		};

		$scope.addNewProblemNew = function(){
			if($scope.newProblemNew.problemname && $scope.newProblemNew.problemname.trim() != ""){
				$scope.newBaseProduct.problemsList.push($scope.newProblemNew);
				$scope.newProblemNew = {};
			} else {
				swal("Oops...", "Problem name is required.", "error");
			}
		};
		$scope.removeProblemNew = function(index){
			$scope.newBaseProduct.problemsList.splice(index, 1);
		};

		$scope.addNewAccessoryNew = function(){
			if($scope.newAccessoryNew.accessoryname && $scope.newAccessoryNew.accessoryname.trim() != ""){
				$scope.newBaseProduct.accessoriesList.push($scope.newAccessoryNew);
				$scope.newAccessoryNew = {};	
			} else {
				swal("Oops...", "Accessory name is required.", "error");
			}
		};
		$scope.removeAccessoryNew = function(index){
			$scope.newBaseProduct.accessoriesList.splice(index, 1);
		};

	};

})();