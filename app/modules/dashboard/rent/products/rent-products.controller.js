(function() {
	'use strict';
	angular
	.module('app')
	.controller('RentProductsController', RentProductsController);

	// RentProductsController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	RentProductsController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'network'];

	// function RentProductsController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function RentProductsController($scope, $rootScope, $state, $timeout, network) {
			// Filters

			$scope.filters = {};
			$scope.filters.products = [];
			$scope.filters.brands = [];
			$scope.filters.priceRange = [];
			
			$scope.filters.products = ["Mobile Phones","Desktops", "TV", "Tablets", "I MAC", "Laptops", "Gaming Console", "Home Appliances"];
			$scope.filters.brands = ["Apple","Assus","Dell","Sony","Samsung","HP","Lenovo", "LG"];

			$scope.filters.priceRange = [
			{
				"displayText": "Under Rs. 10,000",
				"minPrice": 0,
				"maxPrice": 10000
			},
			{
				"displayText": "Rs. 10,001 - 30,000",
				"minPrice": 10001,
				"maxPrice": 30000
			},
			{
				"displayText": "Rs. 30,001 - 50,000",
				"minPrice": 30001,
				"maxPrice": 50000
			},
			{
				"displayText": "Rs. 50,001 - 70,000",
				"minPrice": 50001,
				"maxPrice": 70000
			},
			{
				"displayText": "Above Rs. 70,000",
				"minPrice": 70000,
				"maxPrice": 0
			}
			];

			$scope.selectedPriceRange = {};
			$scope.selectedPriceRange.text = "Select Price Range";

			$scope.changePriceRange = function(range){
				$scope.selectedPriceRange.text = range.displayText;
				$scope.selectedPriceRange.minPrice = range.minPrice;
				$scope.selectedPriceRange.maxPrice = range.maxPrice;
			};

			$scope.passFilters = {};

			$scope.getLots = function(){
				$scope.loadingLots = true;
				network.getLots($scope.user.authToken, $scope.passFilters)
				.then(function(res){
					console.log(res);
					
					$scope.lots = res.data.response;
					angular.forEach($scope.lots, function(value, key){
						value.price = value.lotItemDetails[0].perItemPrice * value.lotItemDetails[0].quantityAvailable;
						value.rent = value.lotItemDetails[0].rentalPricePerMonth * value.lotItemDetails[0].quantityAvailable;
					});
					
					$scope.numberOfLots = res.data.numberOfLots;
					$scope.loadingLots = false;
				}, function(err){
					$scope.loadingLots = false;
				});
			};

			$scope.activeLotType = [];
			$scope.changeLotType = function(lotType){
				$scope.activeLotType = [];
				$scope.activeLotType[lotType] = 'pn-gradient';
				$scope.passFilters.lotType = lotType;
				$scope.getLots();
			};
			$scope.changeLotType('rental');

			$scope.clearAllFilters = function(){
				$scope.appliedFilters.products = [];
				$scope.appliedFilters.brands = [];
				$scope.selectedPriceRange = {};
				$scope.selectedPriceRange.text = "Select Price Range";

				$scope.parsedFilters.products = [];
				$scope.parsedFilters.brands = [];

				$scope.passFilters = {};

				$scope.searchQueryParam = "";
				$scope.passFilters.queryParam = "";

				$scope.changeLotType($scope.currentLotType);
			};

			$scope.appliedFilters = {};
			$scope.appliedFilters.products = [];
			$scope.appliedFilters.brands = {};
			$scope.appliedFilters.priceRange = {};

			$scope.parsedFilters = {};
			$scope.parsedFilters.products = [];
			$scope.parsedFilters.brands = [];

			$scope.applyFilters = function(){
				// parsing products filters
				$scope.parsedFilters.products = [];
				angular.forEach($scope.appliedFilters.products, function(value, key){
					if(value == true){
						$scope.parsedFilters.products.push($scope.filters.products[key]);
					}
				});

				// parsing brands filters
				$scope.parsedFilters.brands = [];
				angular.forEach($scope.appliedFilters.brands, function(value, key){
					if(value == true){
						$scope.parsedFilters.brands.push($scope.filters.brands[key]);
					}
				});

				// prepare data for filters
				if($scope.parsedFilters.products.length){
					$scope.passFilters.productCategories = $scope.parsedFilters.products;
				}
				
				if($scope.parsedFilters.brands.length){
					$scope.passFilters.brands = $scope.parsedFilters.brands;
				}

				if($scope.selectedPriceRange.minPrice || $scope.selectedPriceRange.maxPrice){
					$scope.passFilters.minPriceLimit = $scope.selectedPriceRange.minPrice;
					$scope.passFilters.maxPriceLimit = $scope.selectedPriceRange.maxPrice;
				}
				$scope.getLots();
			};

			$scope.searchWithQuery = function(){				
				$scope.passFilters.queryParam = $scope.searchQueryParam;
				$scope.getLots();
			};

			$scope.addLotToWishlist = function(lot){
				console.log(lot);
				$scope.addToWishlist(lot);
			};
			$scope.removeLotFromWishlist = function(lot){
				console.log(lot);
				$scope.removeFromWishlist(lot);
			};
			$scope.changeSortBy = function(type){
				if(type == "PHL"){
					$scope.sortBy = 'price';
					$scope.sortIsReverse = true;
				}
				if(type == "PLH"){
					$scope.sortBy = 'price';
					$scope.sortIsReverse = false;
				}
			};
		};

	})();