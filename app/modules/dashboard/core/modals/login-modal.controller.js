(function() {
	'use strict';
	angular
	.module('app')
	.controller('LoginModalController', LoginModalController);

	// LoginModalController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	LoginModalController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'authService', 'validationService'];

	// function LoginModalController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function LoginModalController($scope, $rootScope, $state, $localStorage, Facebook, authService, validationService) {
			$scope.something = "Test";

			$scope.loginCredentials = {};
			$scope.loginCredentials.email = "";
			$scope.loginCredentials.password = "";

			$scope.FBUser = {};

			$scope.loginUser = function() {
				if ($scope.loginCredentials.email && $scope.loginCredentials.email.trim != "" && validationService.isEmailValid($scope.loginCredentials.email)) {
					if ($scope.loginCredentials.password && $scope.loginCredentials.password != "") {
					// auth service for network call to login api
					$scope.loginSpinnerEnabled = true;
					authService.login($scope.loginCredentials.email, $scope.loginCredentials.password)
					.then(function(res) {
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
								// user found in database / login success
								$localStorage.userName = res.data.response.name;
								$localStorage.userId = res.data.response.userId;
								$localStorage.userEmail = res.data.response.email;
								$localStorage.userType = res.data.response.accountType;
								$localStorage.userMobile = res.data.response.userMobile;
								$localStorage.userAuthToken = res.data.response.authToken;
								swal("Welcome!", "Welcome " + res.data.response.name, "success");
								$scope.loginCredentials = {};
								$state.go("home", {}, {
									reload: true
								});
							} else if (res.data && res.data.status && res.data.status == "error") {
								swal("Not valid credentials", res.data.message, "error");
							}
							$scope.loginSpinnerEnabled = false;
						}, function(err) {
							swal("Oops...", "Something went wrong while getting logging in", "error");
							$scope.loginSpinnerEnabled = false;
						});
				} else {
					// No password Passed
					swal("Oops...", "Please enter your password", "error");
				}
			} else {
				// No email passed / Not a valid email
				swal("Oops...", "Please enter valid email address", "error");
			}
		};

		// Facebook SDK thing
		$scope.FBLogin = function() {
			Facebook.login(function(response) {
				// Do something with response.
				if(response.authResponse){
					$scope.me();
					$scope.FBUser.facebookAccessToken = response.authResponse.accessToken;
				} else{
					// User denied the facebook popup
				}
			}, {
				scope: 'public_profile, email, user_birthday'
			});
		};

		$scope.getLoginStatus = function() {
			Facebook.getLoginStatus(function(response) {
				if (response.status === 'connected') {
					$scope.loggedIn = true;
				} else {
					$scope.loggedIn = false;
				}
			});
		};

		$scope.me = function() {
			Facebook.api('/me', {
				fields: "name, email, gender, birthday, picture.type(large)"
			}, function(response) {
				//console.log("response", response);
				$scope.prepareFBData(response);
			});
		};

		$scope.FBLogout = function() {
			Facebook.logout(function(response) {
			});
		}

		$scope.$watch(function() {
			return Facebook.isReady();
		}, function(newVal) {
			$scope.facebookReady = true;
			//console.log("ho gya bhai facebook thing");
			// $scope.showLoading = false;
		});

		$scope.prepareFBData = function(response){
			// console.log(response);
			$scope.loadingOnLogin = true;
			$scope.FBUser.loginType = "FB";
			$scope.FBUser.facebookUserId = response.id;
			$scope.FBUser.name = response.name;
			$scope.FBUser.email = response.email;
			$scope.FBUser.facebookImageUrl = response.picture.data.url;

			authService.login($scope.FBUser)
			.then(function(res){
				console.log(res);
				if(response.data.status == "success"){
					// $scope.showLoading = false;
					//console.log("after login:", response);
					$localStorage.userName = res.data.response.name;
					$localStorage.userId = res.data.response.userId;
					$localStorage.userEmail = res.data.response.email;
					$localStorage.userType = res.data.response.accountType;
					$localStorage.userMobile = res.data.response.userMobile;
					$localStorage.userAuthToken = res.data.response.authToken;
					$localStorage.loginType = 'FB';
					if(res.data.response.imageUrl){
						$localStorage.imageUrl = res.data.imageUrl;
					}
					swal("Welcome "+ $scope.FBUser.name, "You have successfully logged in!", "success");
					$state.go("home", {}, {reload: true});
				}
				$scope.loadingOnLogin = false;
			}, function(err){
				$scope.loadingOnLogin = false;
				swal("Oops...", "Something went wrong. Please try again.", "error");
			});
		}
	};

})();