(function() {
	'use strict';
	angular
	.module('app')
	.controller('SignupController', SignupController);

	SignupController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'authService'];

		function SignupController($scope, $rootScope, $state, $timeout, authService) {

			$scope.userInfo = {};
			
			$scope.clickedOnSignUp = function(){
				console.log($scope.userInfo);
				if(!$scope.userInfo.buySellPermission){
					swal("Error", "Please select Registration Type Buyer/Seller", "error");
				}else if(!$scope.userInfo.accountType){
					swal("Error", "Please select User Type Individual / Business", "error");
				}else if(!$scope.userInfo.name || $scope.userInfo.name.length ==0){
					swal("Error", "Please enter valid Name", "error");
				}else if(!$scope.userInfo.password || $scope.userInfo.password.length == 0){
					swal("Error", "Please enter valid password", "error");
				}else if(!$scope.userInfo.confirmPassword || $scope.userInfo.confirmPassword.length == 0){
					swal("Error", "Passwords doesn't match", "error");
				}else if($scope.userInfo.confirmPassword != $scope.userInfo.password){
					swal("Error", "Passwords doesn't match", "error");
				}else if(!$scope.userInfo.email || !$scope.validateEmail($scope.userInfo.email)){
					swal("Error", "Please enter valid Email", "error");
				}else if(!$scope.userInfo.mobileNumber || $scope.userInfo.mobileNumber.length == 0){
					swal("Error", "Please enter valid mobile number", "error");
				}else if(!$scope.userInfo.agreedTermsAndConditions){
					swal("Error", "Please read and agree to TnC", "error");
				}else {
					$scope.signingUp = true;
					authService.signup($scope.userInfo)
					.then(function(res) {
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
							if(res.data.message){
								swal({
								  title: "Success",
								  text: res.data.message,
								  type: "success",
								  showCancelButton: false,
								  confirmButtonColor: "#7ec142",
								  confirmButtonText: "OK",
								  closeOnConfirm: true
								},
								function(){
								  	$state.go('home');
								});
							}else{
								swal({
								  title: "Success",
								  text: "Verification mail have been sent to your email. Please verify before login.",
								  type: "success",
								  showCancelButton: false,
								  confirmButtonColor: "#7ec142",
								  confirmButtonText: "OK",
								  closeOnConfirm: true
								},
								function(){
								  $state.go('home');
								});
							}
						} else if (res.data && res.data.status && res.data.status == "error") {
							swal("Oops...", res.data.message, "error");
						}else{
							swal("Oops...", "Unable to parse server response", "error");
						}
						$scope.signingUp = false;
					}, function(err) {
						$scope.signingUp = false;
						swal("Oops...", "Something went wrong. Please try again.", "error");
					});
				}
			}

			$scope.validateEmail = function(email) {
			    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			    return re.test(email);
			}

		};

	})();