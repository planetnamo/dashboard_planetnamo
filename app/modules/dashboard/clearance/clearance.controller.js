(function() {
	'use strict';
	angular
	.module('app')
	.controller('ClearanceController', ClearanceController);

	ClearanceController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'authService', 'network'];

		function ClearanceController($scope, $rootScope, $state, $timeout, authService, network) {

			$scope.pageHeading = "CLEARANCE STOCK";

			$scope.showUploadArea = function(){
				$scope.pageHeading = "UPLOAD STOCK";
				$scope.showUpload = true;
			};

			$scope.showClearance = function(){
				$scope.pageHeading = "CLEARANCE STOCK";
				$scope.showUpload = false;
			};

			$scope.goToSelectCategory = function(){
				$scope.categorySelected = false;
			}

			$scope.getSelectedCategoryId = function(category,cb){
				var prepareData = {};
				prepareData.filters = {};
				prepareData.filters.queryParam = category;
				network.getProductCategories(prepareData)
				.then(function(res){
					console.log(res);
					if(res.data.status && res.data.status.toLowerCase() == 'success'){
						$scope.selectedCategoryId = res.data.response[0]._id;
						if(cb){
							cb();
						}
					} else if(res.data.status && res.data.status.toLowerCase() == 'error'){
						swal('Oops...', res.data.message, "error");
					}
				}, function(err){
					console.log(err);
				});
			};
			
			$scope.selectCategory = function(category){
				$scope.selectedCategory = category;
				$scope.loadingProducts = true;
				$scope.getSelectedCategoryId($scope.selectedCategory, function(){
					$scope.getProductsList = function(){
						network.getProductsInCategory($scope.selectedCategoryId)
						.then(function(res){
							console.log(res);
							if(res.data.status && res.data.status.toLowerCase() == 'success'){
								console.log(res.data);
								$scope.products = res.data.response;
								$scope.categorySelected = true;
								$scope.loadingProducts = false;
							} else if(res.data.status && res.data.status.toLowerCase() == 'error'){
								swal('Oops...', res.data.message, 'error');
								$scope.loadingProducts = false;
							}
						}, function(err){
							console.log(err);
							swal('Oops...', "Something went wrong. Please try again", 'error');
							$scope.loadingProducts = false;
						});
					};
					$scope.getProductsList();
				});
			};

			$scope.uploadFile = function(){
				if($scope.selectedProduct && $scope.selectedProduct.productName){
					console.log($scope.selectedProduct);
				} else {
					swal("Not a valid product", "Please select a valid product in the category.", "error");
				}
			};

			$scope.getLots = function(){
				$scope.loadingLots = true;
				var filters = {
					"lotType": ["clearance"]
				};
				network.getLots($scope.user.authToken, filters)
				.then(function(res){
					console.log(res);
					if(res.data && res.data.status && res.data.status.toLowerCase() == "success"){
						$scope.lots = res.data.response;
					} else if(res.data && res.data.status && res.data.status.toLowerCase() == "error"){
						swal("Oops...", res.data.message, "error");
					}
					$scope.loadingLots = false;
				}, function(err){
					console.log(err);
					swal("Oops...", "Something went wrong. Please try again.", "error");
					$scope.loadingLots = false;
				});
			};
			$scope.getLots();

		};

	})();