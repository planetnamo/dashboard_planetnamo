(function() {
	'use strict';
	angular
		.module('app')
		.controller('MeProfileController', MeProfileController);

	// MeProfileController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	MeProfileController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'network', 'validationService'];

	// function MeProfileController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
	function MeProfileController($scope, $rootScope, $state, $timeout, network, validationService) {
		$scope.$parent.activeClass = [];
		$scope.$parent.activeClass['profile'] = "active";
		$scope.$parent.activeView = 'Profile';

		$scope.loadingData = true;


		$scope.showGreenPointsModal = function() {
			$('#green-points-modal').modal('show');
		}

		$scope.userInfo = {};
		$scope.userEditInfo = {};
		$scope.userEditInfo.address = null;
		$scope.userChangePassword = {};

		$scope.clearPasswordForm = function(){
			$scope.userChangePassword = {};
		};

		$scope.getUserInfo = function() {
			network.getUserInfo($scope.user.authToken)
				.then(function(res) {
					if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
						$scope.userInfo = res.data.response;
						$scope.userEditInfo.name = angular.copy(res.data.response.name);
						$scope.userEditInfo.mobile = angular.copy(res.data.response.mobile);
						$scope.userEditInfo.address = angular.copy(res.data.response.primaryAddress);
						$scope.loadingData = false;
					} else if (res.data && res.data.status && res.data.status == "error") {
						swal("Oops...", res.data.message, "error");
					}
				}, function(err) {
					if(err.status == 401){
						$scope.logoutUser();
						// swal("Unauthorized", "You are not authorized to perform this action", "error");
					} else {
						swal("Oops...", "Something went wrong. Please try again.", "error");
					}
				});
		};
		$scope.getUserInfo();

		// network call to edit user info
		// after save button is clicked
		$scope.editUserInfo = function(data) {
			$scope.loadingOnUserEdit = true;
			network.editUserInfo($scope.user.authToken, data)
				.then(function(res) {
					console.log(res);
					if(res.data && res.data.status && res.data.status.toLowerCase() == "success"){
						swal("Updated...", "Your information has been updated successfully", "success");
						$state.reload();
					} else if(res.data && res.data.status && res.data.status.toLowerCase() == "error"){
						swal("Oops...", res.data.message, "error");
					}
					$scope.loadingOnUserEdit = false;
				}, function(err) {
					if(err.status == 401){
						swal("Unauthorized", "You are not authorized to perform this action", "error");
					} else {
						swal("Oops...", "Something went wrong. Please try again.", "error");
					}
					$scope.loadingOnUserEdit = false;
				});
		};

		// called when user clicks on save button
		// in edit user info form
		$scope.saveUserInfo = function(){
			var prepareData = {};
			if($scope.userEditInfo.name && $scope.userEditInfo.name.trim() != ""){
				prepareData.name = $scope.userEditInfo.name;
			} else {
				swal("Oops...", "Please enter your name", "error");
				return  false;
			}
			if($scope.userEditInfo.mobile && $scope.userEditInfo.mobile.trim() != ""){
				if(validationService.isPhoneValid($scope.userEditInfo.mobile)){
					prepareData.mobileNumber = $scope.userEditInfo.mobile;
				} else {
					swal("Oops...", "Please enter a valid mobile number.", "error");
					return false;
				}
			} else {
				swal("Oops...", "Please enter your mobile number.", "error");
				return false;
			}
			if($scope.userEditInfo.address.line1 && $scope.userEditInfo.address.line1.trim() != ""){
				prepareData.line1 = $scope.userEditInfo.address.line1;
			} else {
				swal("Oops...", "Please enter your address line 1.", "error");
				return false;
			}
			if($scope.userEditInfo.address.city && $scope.userEditInfo.address.city.trim() != ""){
				prepareData.city = $scope.userEditInfo.address.city;
			} else {
				swal("Oops...", "Please enter your city.", "error");
				return false;
			}
			if($scope.userEditInfo.address.state && $scope.userEditInfo.address.state.trim() != ""){
				prepareData.state = $scope.userEditInfo.address.state;
			} else {
				swal("Oops...", "Please enter your state.", "error");
				return false;
			}
			if($scope.userEditInfo.address.country && $scope.userEditInfo.address.country.trim() != ""){
				prepareData.country = $scope.userEditInfo.address.country;
			} else {
				swal("Oops...", "Please enter your country.", "error");
				return false;
			}
			if($scope.userEditInfo.address.zipCode && $scope.userEditInfo.address.zipCode.trim() != ""){
				prepareData.zipCode = $scope.userEditInfo.address.zipCode;
			} else {
				swal("Oops...", "Please enter your zip code.", "error");
				return false;
			}

			// optional params
			prepareData.line2 = $scope.userEditInfo.address.line2;
			prepareData.locality = $scope.userEditInfo.address.locality;

			console.clear();
			console.log(prepareData);

			$scope.editUserInfo(prepareData);
		};

		$scope.changePassword = function() {
			var prepareData = {};
			if($scope.userChangePassword.current && $scope.userChangePassword.current.trim() != ''){
				prepareData.oldPassword = $scope.userChangePassword.current;
			} else {
				swal("Oops...", "Please enter current password.", "error");
				return false;
			}
			if($scope.userChangePassword.new && $scope.userChangePassword.new.trim() != ''){
			} else {
				swal("Oops...", "Please enter new password.", "error");
				return false;
			}
			if($scope.userChangePassword.confirm && $scope.userChangePassword.confirm.trim() != ''){
				if($scope.userChangePassword.new == $scope.userChangePassword.confirm){
					prepareData.newPassword = $scope.userChangePassword.new;
				} else {
					swal("Oops...", "Confirm assword does not match with new password.", "error");
					return false;
				}
			} else {
				swal("Oops...", "Please confirm new password.", "error");
				return false;
			}

			network.changePassword($scope.user.authToken, prepareData)
				.then(function(res) {
					if(res.data.status && res.data.status.toLowerCase() == "error"){
						swal("Oops...", res.data.message, "error");
					}
				}, function(err) {

				});
		};

		$scope.changeProfilePicture = function() {
			var fd = new FormData();
			fd.append('photo', document.getElementById('user-profile-picture').files[0]);

			if (document.getElementById('user-profile-picture').files[0]) {
				$scope.loadingOnProfilePicture = true;
				network.uploadProfilePicture($scope.user.authToken, fd)
					.then(function(res) {
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
							$scope.userInfo.profilePictueURL = res.data.imageURL;
							
							swal("",res.data.message, "success");
							$state.go('me.profile',{}, {reload:true});
							$('#user-profile-picture').val("");
						} else if (res.data && res.data.status && res.data.status == "error") {
							swal("Oops...", res.data.message, "error");
						}
						$scope.loadingOnProfilePicture = false;
					}, function(err) {
						if(err.status == 401){
							swal("Unauthorized", "You are not authorized to perform this action", "error");
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
						}
						$scope.loadingOnProfilePicture = false;
					});
			} else {
				swal("Oops...", "Please select an image first.", "error");
			}
		};
	};

})();