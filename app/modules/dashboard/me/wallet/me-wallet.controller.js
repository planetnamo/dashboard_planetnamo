(function() {
	'use strict';
	angular
	.module('app')
	.controller('MeWalletController', MeWalletController);

	// MeWalletController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	MeWalletController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'network'];

	// function MeWalletController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function MeWalletController($scope, $rootScope, $state, $timeout, network) {


			$scope.$parent.activeClass = [];
			$scope.$parent.activeClass['wallet'] = "active";
			$scope.$parent.activeView = 'My Wallet';

			$scope.indexController = $scope.$parent.$parent;
			$scope.meController = $scope.$parent;


			$scope.paymentConfigAddToWallet = {
				"key": $scope.indexController.razorPayKey,
				"name": "PlanetNamo Wallet",
				"description": "Wallet Recharge",
				"image": "assets/imgs/planet-namo-logo.png",
				"handler": function(response) {
					$scope.razorpayResponseAddToWallet(response);
				},
				"prefill": {
				},
				"theme": {
					"color": "#3bb549"
				}
			};

			$scope.userWalletAmountInfo = {
				"totalAmount": "",
				"frozenAmount": "",
				"walletAmount":""
			};

			$scope.userBankDetails = {
				"beneficiaryName": "",
				"bankName": "",
				"IFSCCode":"",
				"beneficiaryAccountNumber":"",
				"branchName":"",
				"city":""
			};

			$scope.addMoneyToWallet = {
				"amount": 0,
				"mobile":$scope.indexController.user.mobile,
				"email":$scope.indexController.user.email
			}

			$scope.withdrawAmountInfo = {
				"amount": 0
			}


			$scope.showCards = [];

			$scope.switchCardsView = function(view){
				$scope.showCards = [];
				$scope.showCards[view] = true;
			};

			$scope.switchCardsView('withdraw'); // Init the default view

			$scope.showAddMoney = function(){
				$scope.switchCardsView('addMoney');
			};

			$scope.showWithdraw = function(){
				$scope.switchCardsView('withdraw');
			};

			$scope.showPaymentHistory = function(){
				$scope.switchCardsView('paymentHistory');
				$scope.getTransactionHistory();
			};

			$scope.clickedOnAddMoneyToWallet = function(){
				if($scope.addMoneyToWallet.amount && !isNaN(parseFloat($scope.addMoneyToWallet.amount))){
					if($scope.addMoneyToWallet.mobile && $scope.addMoneyToWallet.mobile.length > 0){
						if($scope.addMoneyToWallet.email && $scope.addMoneyToWallet.email.length > 0){
							$scope.paymentConfigAddToWallet.amount = parseInt(parseFloat($scope.addMoneyToWallet.amount) * 100); //Converting to paise
							$scope.paymentConfigAddToWallet.prefill.name = $scope.indexController.user.name;
							$scope.paymentConfigAddToWallet.prefill.contact = $scope.addMoneyToWallet.mobile;
							$scope.paymentConfigAddToWallet.prefill.mobile = $scope.addMoneyToWallet.mobile;
							$scope.paymentConfigAddToWallet.prefill.email = $scope.addMoneyToWallet.email;
							$scope.instance = new Razorpay($scope.paymentConfigAddToWallet);
							$scope.instance.open();
						}else{
							swal("Oops...", "Please enter valid email", "error");
						}
					}else{
						swal("Oops...", "Please enter valid Mobile Number", "error");
					}
				}else{
					swal("Oops...", "Please enter valid amount", "error");
				}
			}

			$scope.razorpayResponseAddToWallet = function(response){
				if (response.razorpay_payment_id) {
					$scope.addingAmountToWallet = true;
					var apiData = {
						"razorpayPaymentID": response.razorpay_payment_id,
						"amount": parseInt(parseFloat($scope.addMoneyToWallet.amount) * 100) // Converting to paise
					};
					network.addAmountToWallet($scope.indexController.user.authToken, apiData)
					.then(function(res) {
						// console.log(res);
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
							$scope.addingAmountToWallet = false;
							swal("Added!", res.data.successData, "success");
							$scope.getWalletInfo(function(){

							});
						} else if(res.data && res.data.error){
							swal("Oops...", res.data.error, "error");
							$scope.addingAmountToWallet = false;
						}else{
							swal("Oops...", "Unable to add amount to your wallet. Please contact our customer care", "error");
							$scope.addingAmountToWallet = false;
						}
					}, function(err) {
						swal("Oops...", "Something went wrong while processing your payment.", "error");
						$scope.addingAmountToWallet = false;
					});
				} else if (response.error_code) {
					// console.log(response.error_code);
					swal("Oops...", "Unable to process your payment request. Please try again.", "error");
				}
			}

			$scope.updateWithdrawlAccountDetails = function(){
				if($scope.userBankDetails){
					if($scope.userBankDetails.beneficiaryName && $scope.userBankDetails.beneficiaryName.length>0){
						if($scope.userBankDetails.bankName && $scope.userBankDetails.bankName.length>0){
							if($scope.userBankDetails.IFSCCode && $scope.userBankDetails.IFSCCode.length>0){
								if($scope.userBankDetails.beneficiaryAccountNumber && $scope.userBankDetails.beneficiaryAccountNumber.length>0){
									if($scope.userBankDetails.city && $scope.userBankDetails.city.length>0){
										if($scope.userBankDetails.branchName && $scope.userBankDetails.branchName.length>0){
											$scope.updatingBankAccountDetails = true;
											network.updateWithdrawlAccountDetails($scope.indexController.user.authToken, {"bankData": $scope.userBankDetails})
											.then(function(res) {
												console.log(res);
												if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
													$scope.updatingBankAccountDetails = false;
													swal("Added!", "Bank Details Updated Successfully!", "success");
												} else if(res.data && res.data.error){
													swal("Oops...", res.data.error, "error");
													$scope.updatingBankAccountDetails = false;
												}else{
													swal("Oops...", "Unable to update details. Please try again.", "error");
													$scope.updatingBankAccountDetails = false;
												}
											}, function(err) {
												swal("Oops...", "Something went wrong while updating your bank details. Please try again.", "error");
												$scope.updatingBankAccountDetails = false;
											});
										}else{
											swal("Oops...", "Invalid Branch Name", "error");
										}
									}else{
										swal("Oops...", "Invalid City", "error");
									}
								}else{
									swal("Oops...", "Invalid Accunt Number", "error");
								}
							}else{
								swal("Oops...", "Invalid IFSC Code", "error");
							}
						}else{
							swal("Oops...", "Invalid Bank Name", "error");
						}
					}else{
						swal("Oops...", "Invalid Beneficiary Name", "error");
					}
				}else{
					swal("Oops...", "Invalid Bank Details", "error");
				}
			}

			$scope.requestForAmountWithdraw = function(){
				if($scope.userBankDetails){
					if($scope.userBankDetails.beneficiaryName && $scope.userBankDetails.beneficiaryName.length>0){
						if($scope.userBankDetails.bankName && $scope.userBankDetails.bankName.length>0){
							if($scope.userBankDetails.IFSCCode && $scope.userBankDetails.IFSCCode.length>0){
								if($scope.userBankDetails.beneficiaryAccountNumber && $scope.userBankDetails.beneficiaryAccountNumber.length>0){
									if($scope.userBankDetails.city && $scope.userBankDetails.city.length>0){
										if($scope.userBankDetails.branchName && $scope.userBankDetails.branchName.length>0){
											if($scope.withdrawAmountInfo.amount && !isNaN(parseFloat($scope.withdrawAmountInfo.amount))){
												$scope.requestingAmountWithdrawl = true;
												network.requestForAmountWithdraw($scope.indexController.user.authToken, $scope.withdrawAmountInfo)
												.then(function(res) {
													// console.log(res);
													if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
														$scope.requestingAmountWithdrawl = false;
														$scope.withdrawAmountInfo = {
															"amount": 0
														}
														swal("Done", "Withdrawl request placed successfully. Our representative will get back to you shortly.", "success");
													} else if(res.data && res.data.error){
														swal("Oops...", res.data.error, "error");
														$scope.requestingAmountWithdrawl = false;
													}else{
														swal("Oops...", "Unable to place your withdrawl request. Please try again.", "error");
														$scope.requestingAmountWithdrawl = false;
													}
												}, function(err) {
													swal("Oops...", "Something went wrong while processing your request. Please try again.", "error");
													$scope.requestingAmountWithdrawl = false;
												});
											}else{

											}
										}else{
											swal("Oops...", "Invalid Branch Name. Update your bank details first.", "error");
										}
									}else{
										swal("Oops...", "Invalid City. Update your bank details first.", "error");
									}
								}else{
									swal("Oops...", "Invalid Accunt Number. Update your bank details first.", "error");
								}
							}else{
								swal("Oops...", "Invalid IFSC Code. Update your bank details first.", "error");
							}
						}else{
							swal("Oops...", "Invalid Bank Name. Update your bank details first.", "error");
						}
					}else{
						swal("Oops...", "Invalid Beneficiary Name. Update your bank details first.", "error");
					}
				}else{
					swal("Oops...", "Update your bank details first.", "error");
				}
			}

			$scope.updateAllData = function(){
				$scope.getWalletInfo(function(){
					$scope.getUserBankDetails(function(){
						$scope.loadingData = false;
					});
				});
			}

			$scope.getWalletInfo = function(cb) {
				network.getWalletAmountOfUser($scope.indexController.user.authToken)
					.then(function(res) {
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
							// console.log(res.data);
							$scope.userWalletAmountInfo = angular.copy(res.data.response);
							cb();
						} else if (res.data && res.data.status && res.data.status == "error") {
							swal("Oops...", res.data.message, "error");
							cb();
						}else{
							cb();
						}
					}, function(err) {
						if(err.status == 401){
							$scope.logoutUser();
							// swal("Unauthorized", "You are not authorized to perform this action", "error");
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
							cb();
						}
					});
			};

			$scope.getUserBankDetails = function(cb) {
				network.getUserBankDetails($scope.indexController.user.authToken)
					.then(function(res) {
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
							// console.log(res.data);
							if(res.data.response && res.data.response.bankDetails){
								$scope.userBankDetails = angular.copy(res.data.response.bankDetails);
							}
							cb();
						} else if (res.data && res.data.status && res.data.status == "error") {
							swal("Oops...", res.data.message, "error");
							cb();
						}else{
							cb();
						}
					}, function(err) {
						if(err.status == 401){
							$scope.logoutUser();
							// swal("Unauthorized", "You are not authorized to perform this action", "error");
						} else {
							cb();
							swal("Oops...", "Something went wrong. Please try again.", "error");
						}
					});
			};

			$scope.getTransactionHistory = function(){
				$scope.requestingTransactionHistory = true;
				network.getUserWalletTransactionHistory($scope.indexController.user.authToken)
					.then(function(res) {
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
							$scope.requestingTransactionHistory = false;
							// console.log(res.data);
							if(res.data.walletTransactions){
								$scope.userWalletTransactionHistory = angular.copy(res.data.walletTransactions);
							}
						} else if (res.data && res.data.status && res.data.status == "error") {
							swal("Oops...", res.data.message, "error");
							$scope.requestingTransactionHistory = false;
						}else{
							$scope.requestingTransactionHistory = false;
						}
					}, function(err) {
						if(err.status == 401){
							$scope.logoutUser();
							$scope.requestingTransactionHistory = false;
							// swal("Unauthorized", "You are not authorized to perform this action", "error");
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
							$scope.requestingTransactionHistory = false;
						}
					});
			}

			$scope.parseDateToReadableFormat = function(dateString){
				if(dateString){
					try{
						return moment(dateString).format("LLLL");
					}catch(err){

					}
				}
				return "";
			}

			$scope.loadingData = true;
			$scope.updateAllData();


		};

	})();