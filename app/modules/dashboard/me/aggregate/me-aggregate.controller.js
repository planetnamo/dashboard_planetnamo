(function() {
	'use strict';
	angular
	.module('app')
	.controller('MeAggregateController', MeAggregateController);

	// MeAggregateController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	MeAggregateController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'network'];

	// function MeAggregateController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function MeAggregateController($scope, $rootScope, $state, $timeout, network) {
			$scope.$parent.activeClass = [];
			$scope.$parent.activeClass['aggregate'] = "active";

			$scope.indexController = $scope.$parent.$parent;
			$scope.meController = $scope.$parent;

			$scope.loadingData = false;

			$scope.initFlipClockTimeLeft = function(id, seconds){
				$timeout(function(){
					// console.log("called: "+ id + " and "+ seconds);
					$('#countdown-time-'+id).FlipClock(seconds,{
						clockFace: 'HourlyCounter',
						countdown: true
					});
				},100);
			};

			$scope.aggregateOrders = [];

			$scope.getAllAggregateOrders = function(postData, cb){
				// console.log(postData);
				network.getAggregateOrdersOfUser($scope.indexController.user.authToken, postData)
					.then(function(res) {
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
							if(res.data.response && res.data.response.length > 0){
								$scope.aggregateOrders = $scope.aggregateOrders.concat(res.data.response);
							}else{
								$scope.loadMoreDataFromServer = false;
							}
							$scope.refreshFlipClocks();
							// console.log($scope.aggregateOrders);
							cb();
						} else if (res.data && res.data.status && res.data.status == "error") {
							swal("Oops...", res.data.message, "error");
							$scope.loadMoreDataFromServer = false;
							cb();
						}else{
							$scope.loadMoreDataFromServer = false;
							cb();
						}
					}, function(err) {
						$scope.loadMoreDataFromServer = false;
						if(err.status == 401){
							$scope.logoutUser();
							cb();
							// swal("Unauthorized", "You are not authorized to perform this action", "error");
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
							cb();
						}
					});
			}


			$scope.reloadAllPageData = function(){
				$scope.loadingData = true;
				var postData = {};
				postData.limit = 30;
				$scope.aggregateOrders = [];
				$scope.getAllAggregateOrders(postData, function(){
					$scope.loadingData = false;
				});	
			}

			// $scope.reloadAllPageData();

			$scope.loadMoreDataFromServer = true;

			$scope.loadMoreAggregateData = function(){
				if($scope.loadMoreDataFromServer && !$scope.loadingMoreData){
					$scope.loadingMoreData = true;
					var postData = {};
					postData.limit = 30;
					if($scope.aggregateOrders && $scope.aggregateOrders.length > 0 && $scope.aggregateOrders[$scope.aggregateOrders.length - 1].createdAt){
						postData.createdOnBefore = $scope.aggregateOrders[$scope.aggregateOrders.length - 1].createdAt;
					}
					$scope.getAllAggregateOrders(postData, function(){
						$scope.loadingMoreData = false;
					});	
				}
			}

			$scope.refreshFlipClocks = function(){
				$timeout(function(){
					angular.forEach($scope.aggregateOrders, function(val, index){
						if(val.timeLeftLocal){
							if(val.timeLeftLocal/(60*60*24*1000)>1){
								$scope.aggregateOrders[index].daysLeft = parseInt(val.timeLeftLocal/(60*60*24*1000));
							}else{
								$scope.initFlipClockTimeLeft(index, parseInt(val.timeLeftLocal/1000));
							}
						}
					});
				},1000);
			}

			$timeout(function(){
				$(function () {
					$('[data-toggle="tooltip"]').tooltip()
				})
			}, 1000);

			$scope.getTotalQuantity = function(data){
				if(data.orderDetails[0].quantitySold && data.orderDetails[0].quantityLeft){
					return data.orderDetails[0].quantitySold + data.orderDetails[0].quantityLeft;
				}else if(data.orderDetails[0].quantitySold){
					return data.orderDetails[0].quantitySold;
				}else if(data.orderDetails[0].quantityLeft){
					return data.orderDetails[0].quantityLeft;
				}else{
					return 0;
				}
			}

			$scope.buyAggregateObject = null;
			$scope.buyAggregateObjectIndex = -1;
			$scope.clickedOnBuy = function(data, index){
				$scope.buyAggregateObject = data;
				$scope.buyAggregateObjectIndex = index;
				$scope.showSchedulePickupModal(data, index);
			}

			$scope.showSchedulePickupModal = function(data, index){
				$('.schedule-pickup-modal').modal('toggle');
				$scope.loadingAddress = true;
				network.getUserInfo($scope.indexController.user.authToken)
				.then(function(res){
					if(res.data.status && res.data.status.toLowerCase() == 'success'){
						$scope.userAddress = res.data.response.address;
						// console.log($scope.userAddress);
						$scope.showSavedAddresses();
					} else if(res.data.status && res.data.status.toLowerCase() == 'error'){
						swal('Oops...', res.data.message, 'error');
					}
					$scope.loadingAddress = false;
				}, function(err){
					if(err.status == 401){
						swal("Unauthorized", "You are not authorized to perform this action", "error");
					} else {
						swal("Oops...", "Something went wrong. Please try again.", "error");
					}
				});
			};

			$scope.selectedAddressId = null;
			$scope.activeAddress = [];
			$scope.selectAddress = function(addressId){
				$scope.activeAddress = [];
				$scope.activeAddress[addressId] = 'active';
				$scope.selectedAddressId = addressId;
			};

			$scope.addNewAddress = function(){
				$scope.viewSavedAddresses = false;
				$scope.activeAddress = [];
				$scope.selectedAddressId = null;
			};
			$scope.showSavedAddresses = function(){
				$scope.viewSavedAddresses = true;
			};

			$scope.confirmBuy = function(){
				$('.schedule-pickup-modal').modal('hide');
				if($scope.buyAggregateObject != null && $scope.buyAggregateObjectIndex>=0 && $scope.selectedAddressId != null){
					$scope.aggregateOrders[$scope.buyAggregateObjectIndex].buyingOrBackOutLoading = true;
					var postData = {};
					postData.buyingOrderId = $scope.aggregateOrders[$scope.buyAggregateObjectIndex].buyingOrderId;
					// console.log(postData);
					// console.log($scope.indexController.user.authToken);
					network.buyAggregateOrder($scope.indexController.user.authToken, postData)
					.then(function(res) {
						console.log(res);
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
							$scope.aggregateOrders[$scope.buyAggregateObjectIndex].buyingOrBackOutLoading = false;
							$scope.aggregateOrders[$scope.buyAggregateObjectIndex].totalAmountPaid = true;
							swal("Success", "Congrats on your new product purchase. Thanks for shopping with us", "success");
						} else if (res.data && res.data.status && res.data.status == "error") {
							swal("Oops...", res.data.message, "error");
							$scope.aggregateOrders[$scope.buyAggregateObjectIndex].buyingOrBackOutLoading = false;
						}else{
							$scope.aggregateOrders[$scope.buyAggregateObjectIndex].buyingOrBackOutLoading = false;
						}
						$scope.buyAggregateObject = null;
						$scope.buyAggregateObjectIndex = -1;
					}, function(err) {
						$scope.aggregateOrders[$scope.buyAggregateObjectIndex].buyingOrBackOutLoading = false;
						if(err.status == 401){
							$scope.logoutUser();
							// swal("Unauthorized", "You are not authorized to perform this action", "error");
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
						}
						$scope.buyAggregateObject = null;
						$scope.buyAggregateObjectIndex = -1;
					});
				}else if($scope.buyAggregateObject == null){
					swal("Order Info", "Unable to determine Order info. Please try again.", "warning");
				}else if($scope.buyAggregateObjectIndex < 0){
					swal("Order Info", "Unable to determine Order info. Please try again.", "warning");
				}else if($scope.selectedAddressId == null){
					swal("Address Missing", "Please select delivery address.", "warning");
				}else{
					swal("Error", "Unable to buy the product. Please contact our customer care for more info.", "error");
				}
			}

			$scope.backOutClicked = function(data, index){
				if($scope.aggregateOrders && $scope.aggregateOrders.length > index && $scope.aggregateOrders[index].buyingOrderId){
					swal({
					  title: "Are you sure?",
					  text: "Freezed EMD will forfeited.",
					  type: "warning",
					  showCancelButton: true,
					  confirmButtonColor: "#3c7b44",
					  confirmButtonText: "Proceed",
					  cancelButtonText: "Cancel",
					  closeOnConfirm: true,
					  closeOnCancel: true
					},
					function(isConfirm){
					  if (isConfirm) {
					    $scope.aggregateOrders[index].buyingOrBackOutLoading = true;
						var postData = {};
						postData.buyingOrderId = $scope.aggregateOrders[index].buyingOrderId;
						// console.log(postData);
						// console.log($scope.indexController.user.authToken);
						network.backOutFromAggregateOrder($scope.indexController.user.authToken, postData)
						.then(function(res) {
							console.log(res);
							if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
								$scope.aggregateOrders[index].buyingOrBackOutLoading = false;
								$scope.aggregateOrders[index].cancelBuyingOrder = true;
								swal("Success", "Backed out successfully.", "success");
							} else if (res.data && res.data.status && res.data.status == "error") {
								swal("Oops...", res.data.message, "error");
								$scope.aggregateOrders[index].buyingOrBackOutLoading = false;
							}else{
								$scope.aggregateOrders[index].buyingOrBackOutLoading = false;
							}
						}, function(err) {
							$scope.aggregateOrders[index].buyingOrBackOutLoading = false;
							if(err.status == 401){
								$scope.logoutUser();
								// swal("Unauthorized", "You are not authorized to perform this action", "error");
							} else {
								swal("Oops...", "Something went wrong. Please try again.", "error");
							}
						});
					  } else {
					    
					  }
					});
				}
			}

		};

	})();