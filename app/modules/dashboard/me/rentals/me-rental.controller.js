(function() {
	'use strict';
	angular
	.module('app')
	.controller('MeRentalController', MeRentalController);

	// MeRentalController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	MeRentalController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'network'];

	// function MeRentalController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function MeRentalController($scope, $rootScope, $state, $timeout, network) {
			$scope.$parent.activeClass = [];
			$scope.$parent.activeClass['rentals'] = "active";
			$scope.$parent.activeView = 'My Rentals';

			$scope.selectedNumberOfMonths = null;

			$scope.selectedExtendPeriod = {};
			$scope.changeExtendPeriod = function(rental, value){
				$scope.selectedExtendPeriod[rental.rentalOrderId] = {};
				$scope.selectedExtendPeriod[rental.rentalOrderId][value] = "selected";
			};

			$scope.selectNumberOfMonths = function(rental, months){
				try{
					if(months == 3 || months == 6 || months == 9){
						// If selected number of months is one from predefined
						if(months == 3){
							rental.calculatedRental = rental.orderDetails[0].rentalPriceByNumberOfMonths[0].price * rental.orderDetails[0].quantity;
						} else if(months == 6){
							rental.calculatedRental = rental.orderDetails[0].rentalPriceByNumberOfMonths[1].price * rental.orderDetails[0].quantity;
						} else if(months == 9){
							rental.calculatedRental = rental.orderDetails[0].rentalPriceByNumberOfMonths[2].price * rental.orderDetails[0].quantity;
						}
						rental.selectedNumberOfMonths = parseInt(months);
						rental.numberOfMonthsSelected = true;
						rental.securityDeposit = rental.orderDetails[0].securityDeposit * rental.orderDetails[0].quantity;
						rental.calculatedTotal = rental.securityDeposit + rental.calculatedRental;
						$scope.changeExtendPeriod(rental, months);
					} else if(months != ''){
						// if value is a custom value
						rental.selectedNumberOfMonths = parseInt(months);
						rental.calculatedRental = (rental.orderDetails[0].productPerMonthPrice * rental.orderDetails[0].quantity) * parseInt(months);
						rental.numberOfMonthsSelected = true;
						rental.securityDeposit = rental.orderDetails[0].securityDeposit * rental.orderDetails[0].quantity;
						rental.calculatedTotal = rental.securityDeposit + rental.calculatedRental;
						$scope.changeExtendPeriod(rental, 'custom');
					} else {
						$scope.selectedExtendPeriod = [];
						$scope.numberOfMonthsSelected = false;
					}
				} catch(e){
					$scope.selectedExtendPeriod = [];
					$scope.numberOfMonthsSelected = false;
				}
			};

			$scope.selectedRentalOrder = null;

			$scope.paymentConfigAddToWallet = {
				"key": $scope.razorPayKey,
				"name": "PlanetNamo Wallet",
				"description": "Wallet Recharge",
				"image": "assets/imgs/planet-namo-logo.png",
				"handler": function(response) {
					$scope.razorpayResponseAddToWallet(response);
				},
				"prefill": {
				},
				"theme": {
					"color": "#3bb549"
				}
			};

			$scope.razorpayResponseAddToWallet = function(response){
				if (response.razorpay_payment_id) {
					$scope.addingAmountToWallet = true;
					var apiData = {
						"razorpayPaymentID": response.razorpay_payment_id,
						"amount": parseInt(parseFloat($scope.amountNeededMore) * 100) // Converting to paise
					};

					$scope.selectedRentalOrder.paymentLoading = true;
					
					network.addAmountToWallet($scope.user.authToken, apiData)
					.then(function(res) {
						// console.log(res);
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
							var postData = {};
							postData.lotId = $scope.selectedRentalOrder.lotId;
							postData.monthsToRent = $scope.selectedRentalOrder.selectedNumberOfMonths;
							postData.startRentalDate = moment($scope.selectedRentalOrder.endDate).add(1, 'd').format("YYYY-MM-DD");
							postData.orderDetails = $scope.selectedRentalOrder.orderDetailsDict;
							postData.deliveryAddressId = $scope.selectedRentalOrder.deliveryAddressId;

							network.placeRentalOrder($scope.user.authToken, postData)
							.then(function(res){
								console.log(res);
								if(res.data.status && res.data.status.toLowerCase() == 'success'){
									swal("Success", "Order Placed Successfully.", "success");
									$scope.selectedRentalOrder = null;
									$state.go("me.rentals",{},{reload: true});
								}else if(res.data.status && res.data.status.toLowerCase() == 'error' && res.data.amountNeededMore){
									swal("Alert", "", "success");
								} else if (res.data && res.data.status && res.data.status == "error") {
									swal("Oops...", res.data.message, "error");
								}else{
									swal("Oops...", "Unable to reach servers. Please Try Again.", "error");
								}
								$scope.selectedRentalOrder.paymentLoading = false;
								$scope.amountNeededMore = 0;
							}, function(err){
								console.log(err);
								if(err.status == 401){
									$scope.logoutUser();
									// swal("Unauthorized", "You are not authorized to perform this action", "error");
								} else {
									swal("Oops...", "Something went wrong. Please try again.", "error");
								}
								$scope.selectedRentalOrder.paymentLoading = false;
								$scope.amountNeededMore = 0;
							});
						} else if(res.data && res.data.error){
							swal("Oops...", res.data.error, "error");
							$scope.selectedRentalOrder.paymentLoading = false;
							$scope.amountNeededMore = 0;
						}else{
							swal("Oops...", "Unable to add amount to your wallet. Please contact our customer care", "error");
							$scope.selectedRentalOrder.paymentLoading = false;
							$scope.amountNeededMore = 0;
						}
					}, function(err) {
						swal("Oops...", "Something went wrong while processing your payment.", "error");
						$scope.selectedRentalOrder.paymentLoading = false;
						$scope.amountNeededMore = 0;
					});
				} else if (response.error_code) {
					// console.log(response.error_code);
					$scope.amountNeededMore = 0;
					$scope.selectedRentalOrder.paymentLoading = false;
					swal("Oops...", "Unable to process your payment request. Please try again.", "error");
				}
			}

			$scope.extendContract = function(rental){
				if(true){
					swal({
					  title: "Are you sure?",
					  text: "Extending for " + rental.selectedNumberOfMonths + " months. Amount of " + (rental.calculatedTotal) + " will be deducted from your wallet.",
					  type: "warning",
					  showCancelButton: true,
					  confirmButtonColor: "#5cb85c",
					  confirmButtonText: "Yes, Continue",
					  cancelButtonText: "No",
					  closeOnConfirm: true,
					  closeOnCancel: true
					},
					function(isConfirm){
					  if (isConfirm) {
					    rental.paymentLoading = true;

						var postData = {};
						postData.lotId = rental.lotId;
						postData.monthsToRent = rental.selectedNumberOfMonths;
						postData.startRentalDate = moment(rental.endDate).add(1, 'd').format("YYYY-MM-DD");
						postData.orderDetails = rental.orderDetailsDict;
						postData.deliveryAddressId = rental.deliveryAddressId;

						network.placeRentalOrder($scope.user.authToken, postData)
						.then(function(res){
							// console.log(res);
							if(res.data.status && res.data.status.toLowerCase() == 'success'){
								swal("Success", "Order Placed Successfully.", "success");
								$state.go("me.rentals",{},{reload: true});
							}else if(res.data.status && res.data.status == "error" && res.data.amountNeededMore){
								swal({
								  title: "Not sufficient amount in wallet",
								  text: "Recharge wallet with "+res.data.amountNeededMore+" more inorder to continue.",
								  type: "warning",
								  showCancelButton: true,
								  confirmButtonColor: "#5cb85c",
								  confirmButtonText: "Yes, Continue",
								  cancelButtonText: "No",
								  closeOnConfirm: true,
								  closeOnCancel: true
								},
								function(isConfirm){
								  if (isConfirm) {
								  	$scope.selectedRentalOrder = rental;
								    rental.paymentLoading = true;
								    $scope.paymentConfigAddToWallet.amount = parseInt(parseFloat(res.data.amountNeededMore) * 100); //Converting to paise
									$scope.paymentConfigAddToWallet.prefill.name = $scope.user.name;
									$scope.paymentConfigAddToWallet.prefill.contact = $scope.user.mobile;
									$scope.paymentConfigAddToWallet.prefill.mobile = $scope.user.mobile;
									$scope.paymentConfigAddToWallet.prefill.email = $scope.user.email;
									$scope.instance = new Razorpay($scope.paymentConfigAddToWallet);
									$scope.amountNeededMore = res.data.amountNeededMore;
									$scope.instance.open();
								  } else {
								  }
								});
							} else if (res.data && res.data.status && res.data.status == "error") {
								swal("Oops...", res.data.message, "error");
							}else{
								swal("Oops...", "Unable to reach servers. Please Try Again.", "error");
							}
							rental.paymentLoading = false;
						}, function(err){
							console.log(err);
							if(err.status == 401){
								$scope.logoutUser();
								// swal("Unauthorized", "You are not authorized to perform this action", "error");
							} else {
								swal("Oops...", "Something went wrong. Please try again.", "error");
							}
							rental.paymentLoading = false;
						});
					  } else {
					  }
					});
				}else{
					swal("Oops...","Select the address for delivery", "warning");
				}
			};

			$scope.endContract = function(rental){
				swal({
					title: "Are you sure?",
					text: "Do you really want to end this contract?",
					type: "warning",
					showCancelButton: true,
					confirmButtonText: "Yes, End Contract",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm){
				  if (isConfirm) {
				    rental.endContractLoading = true;

					var postData = {};
					postData.rentalOrderId = rental.rentalOrderId;

					network.returnRentalOrder($scope.user.authToken, postData)
					.then(function(res){
						// console.log(res);
						if(res.data.status && res.data.status.toLowerCase() == 'success'){
							swal("Ended!", "Your contract end request has been successfully submitted!", "success");
							$state.go("me.rentals",{},{reload: true});
						} else if (res.data.status && res.data.status == "error") {
							swal("Oops...", res.data.message, "error");
						} else{
							swal("Oops...", "Unable to reach servers. Please Try Again.", "error");
						}
						rental.endContractLoading = false;
					}, function(err){
						console.log(err);
						if(err.status == 401){
							$scope.logoutUser();
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
						}
						rental.endContractLoading = false;
					});
				  } else {
				  }
				});
			};

			


			$scope.rentals = [];

			$scope.getAllRentals = function(postData, cb){
				// console.log(postData);
				network.getRentalsOfUser($scope.user.authToken, postData)
					.then(function(res) {
						console.log(res);
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
							if(res.data.response && res.data.response.length > 0){
								$scope.rentals = $scope.rentals.concat(res.data.response);
								for(var i = $scope.rentals.length; i>= 0; i--){
									$scope.selectNumberOfMonths($scope.rentals[i], '3');
								}
							}else{
								$scope.loadMoreDataFromServer = false;
							}
							// console.log($scope.rentals);
							cb();
						} else if (res.data && res.data.status && res.data.status == "error") {
							swal("Oops...", res.data.message, "error");
							$scope.loadMoreDataFromServer = false;
							cb();
						}else{
							$scope.loadMoreDataFromServer = false;
							cb();
						}
					}, function(err) {
						$scope.loadMoreDataFromServer = false;
						if(err.status == 401){
							$scope.logoutUser();
							cb();
							// swal("Unauthorized", "You are not authorized to perform this action", "error");
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
							cb();
						}
					});
			}


			$scope.reloadAllPageData = function(){
				$scope.loadingData = true;
				var postData = {};
				postData.limit = 30;
				$scope.rentals = [];
				$scope.getAllRentals(postData, function(){
					$scope.loadingData = false;
				});	
			}

			$scope.reloadAllPageData();

			$scope.loadMoreDataFromServer = true;

			$scope.loadMoreRentalsData = function(){
				if($scope.loadMoreDataFromServer && !$scope.loadingMoreData){
					$scope.loadingMoreData = true;
					var postData = {};
					postData.limit = 30;
					if($scope.rentals && $scope.rentals.length > 0 && $scope.rentals[$scope.rentals.length - 1].SellingOrderInfo && $scope.rentals[$scope.rentals.length - 1].SellingOrderInfo.createdAt){
						postData.createdOnBefore = $scope.rentals[$scope.rentals.length - 1].SellingOrderInfo.createdAt;
					}
					$scope.getAllRentals(postData, function(){
						$scope.loadingMoreData = false;
					});	
				}
			}

			$scope.getFormattedDate = function(dateString){
				if(dateString){
					try{
						return moment(dateString).format("LLLL");
					}catch(err){

					}
				}
				return "";
			}


		};

	})();