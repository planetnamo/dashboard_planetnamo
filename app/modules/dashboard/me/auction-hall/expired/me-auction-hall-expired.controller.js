(function() {
	'use strict';
	angular
	.module('app')
	.controller('MeAuctionHallExpiredController', MeAuctionHallExpiredController);

	// MeAuctionHallExpiredController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	MeAuctionHallExpiredController.$inject = ['$scope', '$rootScope', '$state', '$timeout'];

	// function MeAuctionHallExpiredController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function MeAuctionHallExpiredController($scope, $rootScope, $state, $timeout) {
			$scope.$parent.auctionHallActiveClass = [];
			$scope.$parent.auctionHallActiveClass['expired'] = "active";

			$scope.indexController = $scope.$parent.$parent.$parent;
			$scope.meController = $scope.$parent.$parent;
			$scope.auctionController = $scope.$parent;

			$scope.loadMoreExpiredAuctions = function(){
				$scope.auctionController.loadMoreExpiredAuctions(function(errorString){
					
				});
			}
		};

	})();