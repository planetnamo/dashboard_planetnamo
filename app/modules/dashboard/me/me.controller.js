(function() {
	'use strict';
	angular
	.module('app')
	.controller('MeController', MeController);

	// MeController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	MeController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'network'];

	// function MeController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function MeController($scope, $rootScope, $state, $timeout, network) {
			$scope.activeClass = [];
			$scope.activeView = null;

			// console.log($scope.user);

			// Prefixed user type
			// To show user type in info
			$scope.userType = [];
			$scope.userType[1] = "Individual";
			$scope.userType[2] = "Business";

			// if user auth is required for this module
			// redirect to home in case of auth failed
			$scope.userAuthRequired();


			network.getUserInfo($scope.user.authToken)
			.then(function(res){
				if(res.data && res.data.status && res.data.status.toLowerCase() == "success" && $scope.$parent.user && res.data.response){
					for(var key in res.data.response){
						// console.log(key);
						// console.log(res.data.response[key]);
						if(res.data.response[key]){
							$scope.$parent.user[key] = res.data.response[key];
						}
					}
				}
			}, function(err){
				if(err.status == 401){
					$scope.logoutUser();
				} else {
					swal("Oops...", "Something went wrong. Please try again.", "error");
				}
			});

			network.getUserStats($scope.user.authToken)
			.then(function(res){
				if(res.data && res.data.status && res.data.status.toLowerCase() == "success"){
					$scope.stats = res.data.response;
				} else if(res.data && res.data.status && res.data.status.toLowerCase() == "error"){
					swal("Oops...", res.data.message, "error");
				};
			}, function(err){
				if(err.status == 401){
					$scope.logoutUser();
				} else {
					swal("Oops...", "Something went wrong. Please try again.", "error");
				}
			});

		};

	})();