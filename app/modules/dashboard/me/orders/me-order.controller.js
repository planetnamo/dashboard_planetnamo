(function() {
	'use strict';
	angular
	.module('app')
	.controller('MeOrderController', MeOrderController);

	// MeOrderController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	MeOrderController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'network'];

	// function MeOrderController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function MeOrderController($scope, $rootScope, $state, $timeout, network) {
			$scope.$parent.activeClass = [];
			$scope.$parent.activeClass['orders'] = "active";
			$scope.$parent.activeView = 'Order History';

			$scope.indexController = $scope.$parent.$parent;
			$scope.meController = $scope.$parent;

			$scope.invoiceSelectedOrder = null;

			$scope.viewInvoice = function(order){
				$scope.invoiceSelectedOrder = order;
				// Show loader
				// Network call for invoice
				// Fill Data in invoice
				$scope.invoiceViewEnabled = true;
				$timeout(function() {
					$scope.$apply();
				}, 100);
				// Hide loader
			};

			$scope.viewAllOrders = function(){
				// Show loader
				// Set the invoice data null
				$scope.invoiceViewEnabled = false;
				// Hide loader
			};

			$scope.downloadInvoice = function(){
				html2canvas(document.getElementById('invoice-tables'), {
					onrendered: function (canvas) {
						var data = canvas.toDataURL();
						var docDefinition = {
							content: [{
								image: data,
								width: 500,
							}]
						};
						pdfMake.createPdf(docDefinition).download("Planetnamo-Invoice.pdf");
					}
				});
			};

			$scope.printInvoice = function(){
				html2canvas(document.getElementById('invoice-tables'), {
					onrendered: function (canvas) {
						var data = canvas.toDataURL();
						var docDefinition = {
							content: [{
								image: data,
								width: 500,
							}]
						};
						pdfMake.createPdf(docDefinition).print();
					}
				});
			};

			$scope.orders = [];

			$scope.getAllOrders = function(postData, cb){
				// console.log(postData);
				network.getOrderHistoryOfUser($scope.indexController.user.authToken, postData)
					.then(function(res) {
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
							if(res.data.response && res.data.response.length > 0){
								$scope.orders = $scope.orders.concat(res.data.response);
							}else{
								$scope.loadMoreDataFromServer = false;
							}
							cb();
						} else if (res.data && res.data.status && res.data.status == "error") {
							swal("Oops...", res.data.message, "error");
							$scope.loadMoreDataFromServer = false;
							cb();
						}else{
							$scope.loadMoreDataFromServer = false;
							cb();
						}
					}, function(err) {
						$scope.loadMoreDataFromServer = false;
						if(err.status == 401){
							$scope.logoutUser();
							cb();
							// swal("Unauthorized", "You are not authorized to perform this action", "error");
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
							cb();
						}
					});
			}


			$scope.reloadAllPageData = function(){
				$scope.loadingData = true;
				var postData = {};
				postData.limit = 30;
				$scope.orders = [];
				$scope.getAllOrders(postData, function(){
					$scope.loadingData = false;
				});	
			}

			// $scope.reloadAllPageData();

			$scope.loadMoreDataFromServer = true;

			$scope.loadMoreOrdersData = function(){
				if($scope.loadMoreDataFromServer && !$scope.loadingMoreData){
					$scope.loadingMoreData = true;
					var postData = {};
					postData.limit = 30;
					if($scope.orders && $scope.orders.length > 0 && $scope.orders[$scope.orders.length - 1].createdAt){
						postData.createdOnBefore = $scope.orders[$scope.orders.length - 1].createdAt;
					}
					$scope.getAllOrders(postData, function(){
						$scope.loadingMoreData = false;
					});	
				}
			}

			$scope.getOrderStatus = function(buyingOrderFulfilled, cancelBuyingOrder){
				if(buyingOrderFulfilled){
					return "Completed";
				}else if(cancelBuyingOrder){
					return "Cancelled";
				}else{
					return "Received";
				}
			}

			$scope.getFormattedDate = function(dateString){
				if(dateString){
					try{
						return moment(dateString).format("DD MMM YYYY");
					}catch(err){

					}
				}
				return "";
			}

		};

	})();