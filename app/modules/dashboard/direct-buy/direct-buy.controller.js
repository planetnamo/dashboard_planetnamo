(function() {
	'use strict';
	angular
	.module('app')
	.controller('DirectBuyController', DirectBuyController);

	DirectBuyController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'authService', 'network', 'uiGridConstants'];

		function DirectBuyController($scope, $rootScope, $state, $timeout, authService, network, uiGridConstants) {

			$scope.columnsFromExcel = [];
			$scope.dataFromExcel = [];
			$scope.gridOptions = {
				fastWatch: true
			};
			$scope.gridOptions.columnDefs = [
			  ];
			$scope.gridOptions.data = $scope.dataFromExcel;
			$scope.gridApi = null;
			$scope.gridOptions.onRegisterApi = function(gridApi){
			   $scope.gridApi = gridApi; 
			};

			$scope.pageHeading = "DIRECT BUY";
			$scope.selectedCategoryData = {};

			$scope.selectedFile = null;

			$scope.currentlot = null;
			$scope.currentLotIndex = -1;

			$scope.showUploadArea = function(){
				$scope.pageHeading = "UPLOAD PRODUCTS";
				$scope.showUpload = true;
			};

			$scope.showDirectBuy = function(){
				$scope.pageHeading = "DIRECT BUY";
				$scope.showUpload = false;
			};

			$scope.goToSelectCategory = function(){
				$scope.categorySelected = false;
			}

			$scope.getSelectedCategoryId = function(category,cb){
				var prepareData = {};
				prepareData.filters = {};
				prepareData.filters.queryParam = category;
				network.getProductCategories(prepareData)
				.then(function(res){
					if(res.data.status && res.data.status.toLowerCase() == 'success' && res.data.response && res.data.response[0]){
						$scope.selectedCategoryData = res.data.response[0];
						$scope.selectedCategoryId = res.data.response[0]._id;
						if(cb){
							cb();
						}
					} else if(res.data.status && res.data.status.toLowerCase() == 'error'){
						swal('Oops...', res.data.message, "error");
						cb('error');
					}else{
						swal('Oops...', "Category not registered", "error");
						cb('error');
					}
				}, function(err){
					console.log(err);
				});
			};
			
			$scope.selectCategory = function(category){
				$scope.selectedCategory = category;
				$scope.loadingProducts = true;
				$scope.getSelectedCategoryId($scope.selectedCategory, function(error){
					if(error){
						$scope.goToSelectCategory();
					}else{
						// $scope.getProductsList = function(){
						// 	network.getProductsInCategory($scope.selectedCategoryId)
						// 	.then(function(res){
						// 		if(res.data.status && res.data.status.toLowerCase() == 'success'){
									// $scope.products = res.data.response;
									$scope.categorySelected = true;
									$scope.loadingProducts = false;
						// 		} else if(res.data.status && res.data.status.toLowerCase() == 'error'){
						// 			swal('Oops...', res.data.message, 'error');
						// 			$scope.loadingProducts = false;
						// 		}
						// 	}, function(err){
						// 		console.log(err);
						// 		swal('Oops...', "Something went wrong. Please try again", 'error');
						// 		$scope.loadingProducts = false;
						// 	});
						// };
						// $scope.getProductsList();
					}
				});
			};

			$scope.fileChanged = function($event){
				$scope.selectedFile = $event.target.files[0];
			}

			$scope.getLots = function(){
				$scope.loadingLots = true;
				var filters = {
					"lotType": ["buy"]
				};
				network.getLots($scope.user.authToken, filters)
				.then(function(res){
					console.log(res);
					if(res.data && res.data.status && res.data.status.toLowerCase() == "success"){
						$scope.lots = res.data.response;
					} else if(res.data && res.data.status && res.data.status.toLowerCase() == "error"){
						swal("Oops...", res.data.message, "error");
					}
					$scope.loadingLots = false;
				}, function(err){
					console.log(err);
					swal("Oops...", "Something went wrong. Please try again.", "error");
					$scope.loadingLots = false;
				});
			};
			$scope.getLots();

			$scope.uploadFile = function(){
				if($scope.selectedFile){
					$scope.readExcelFile($scope.selectedFile);
				}else{
					swal("Error", "No file found for data upload.", "error");
				}
			};


			$scope.readExcelFile = function(file){
				var reader = new FileReader();

				reader.onload = function (e) {
					/* read workbook */
					var bstr = e.target.result;
					var wb = XLSX.read(bstr, {type:'binary'});

					/* grab first sheet */
					var wsname = wb.SheetNames[0];
					var ws = wb.Sheets[wsname];

					/* grab first row and generate column headers */
					var aoa = XLSX.utils.sheet_to_json(ws, {header:1, raw:false});
					$scope.columnsFromExcel = [];
					$scope.gridOptions.columnDefs = [];
					for(var i = 0; i < aoa[0].length; ++i){
						$scope.columnsFromExcel[i] = { field: aoa[0][i] };
						$scope.gridOptions.columnDefs.push({"name":aoa[0][i], "width": 10*(aoa[0][i].length) + 25});
					}

					/* generate rest of the data */
					$scope.dataFromExcel = [];
					for(var r = 1; r < aoa.length; ++r) {
						$scope.dataFromExcel[r-1] = {};
						for(i = 0; i < aoa[r].length; ++i) {
							if(aoa[r][i] == null) continue;
							$scope.dataFromExcel[r-1][aoa[0][i]] = aoa[r][i]
						}
					}

					$scope.$apply(function(){
						$scope.gridOptions.data = $scope.dataFromExcel;
						$timeout(function() {
							$scope.gridApi.core.refresh();
							$scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.ALL);
						}, 100);
					});
				};

				reader.readAsBinaryString(file);
			}


			$scope.downloadExcelForCategory = function(){

				var wopts = { bookType: 'xlsx', bookSST: true, type: 'binary' };
				var fileName = $scope.selectedCategoryData.categoryName || 'SheetJS';
				fileName += wopts.bookType ? "." + wopts.bookType : '.xlsx';

				var sheetName = 'Sheet1';

				var columns = ["Product Name", "Brand Name", "Model Number", "PlanetNamo Cost", "Warranty And Support", "Age in Months", "Number of Pieces"];

				if($scope.selectedCategoryData.accessories){
					for(var i=0; i<$scope.selectedCategoryData.accessories.length; i++){
						columns.push($scope.selectedCategoryData.accessories[i].accessoryname);
					}
				}

				if($scope.selectedCategoryData.specifications){
					for(var i=0; i<$scope.selectedCategoryData.specifications.length; i++){
						columns.push($scope.selectedCategoryData.specifications[i].displayName);
					}
				}

				if($scope.selectedCategoryData.problems){
					for(var i=0; i<$scope.selectedCategoryData.problems.length; i++){
						columns.push($scope.selectedCategoryData.problems[i].problemname);
					}
				}

				var wb = XLSX.utils.book_new(), ws = $scope.uigrid_to_sheet(columns);
				wb.SheetNames.push("Sheet1");
				wb.Sheets["Sheet1"] = ws;
				var wbout = XLSX.write(wb, wopts);
				saveAs(new Blob([$scope.s2ab(wbout)], { type: 'application/octet-stream' }), fileName);
			}

			$scope.uigrid_to_sheet = function(columns){
				var o = [], oo = [], i = 0, j = 0;

				/* column headers */
				for(j = 0; j < columns.length; ++j) oo.push(columns[j]);
				o.push(oo);
				/* aoa_to_sheet converts an array of arrays into a worksheet object */
				return XLSX.utils.aoa_to_sheet(o);
			}

			$scope.s2ab = function(s) {
				if(typeof ArrayBuffer !== 'undefined') {
					var buf = new ArrayBuffer(s.length);
					var view = new Uint8Array(buf);
					for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
					return buf;
				} else {
					var buf = new Array(s.length);
					for (var i=0; i!=s.length; ++i) buf[i] = s.charCodeAt(i) & 0xFF;
					return buf;
				}
			}


			$scope.uploadData = function(){
				if($scope.selectedCategoryId && $scope.dataFromExcel.length > 0){
					$scope.loadingLots = true;
					var postData = {};
					postData.baseProductsInfo = [];
					postData.categoryId = $scope.selectedCategoryId;
					for(var i=0; i<$scope.dataFromExcel.length; i++){
						var localPushObj = {};
						var localExcelObj = $scope.dataFromExcel[i];
						localPushObj.productName = localExcelObj['Product Name'];
						localPushObj.brandName = localExcelObj['Brand Name'];
						localPushObj.modelNumber = localExcelObj['Model Number'];
						localPushObj.planetNamoCost = localExcelObj['PlanetNamo Cost'];
						localPushObj.warrantyNSupport = localExcelObj['Warranty And Support'];
						localPushObj.ageInMonths = localExcelObj['Age in Months'];
						localPushObj.noOfPieces = localExcelObj['Number of Pieces'];
						localPushObj.accessoriesList = [];
						localPushObj.specifications = [];
						localPushObj.problemsList = [];
						if($scope.selectedCategoryData.accessories){
							localPushObj.accessoriesList = $scope.selectedCategoryData.accessories;
						}

						if($scope.selectedCategoryData.specifications){
							localPushObj.specifications = $scope.selectedCategoryData.specifications;
						}

						if($scope.selectedCategoryData.problems){
							localPushObj.problemsList = $scope.selectedCategoryData.problems;
						}
						localPushObj.completeExcelAnswers = localExcelObj;
						postData.baseProductsInfo.push(localPushObj);
					}
					network.createFinalBuyingLotFromAdminPanel($scope.user.authToken, postData)
						.then(function(res){
							console.log(res);
							if(res.data && res.data.status && res.data.status == 'success'){
								swal("Success", "Upload Successful.", "success");
							}else if(res.data && res.data.message){
								swal("Oops...", res.data.message, "error");
							}else{
								swal("Oops...", "Something went wrong. Please try again.", "error");
							}
							$scope.loadingLots = false;
						}, function(err){
							console.log(err);
							swal("Oops...", "Something went wrong. Please try again.", "error");
							$scope.loadingLots = false;
						});
				}else if($scope.dataFromExcel.length){
					swal("Error", "No category selected.", "error");
				}else{
					swal("Error", "No data found to upload.", "error");
				}
			};


			$scope.clickedOnView = function(lot, index){
				$scope.currentlot = angular.copy(lot);
				$scope.currentLotIndex = index;
				$scope.modalShown = true;
			}

			$scope.clickedOnDelete = function(lot, index){
				
			}

			$scope.clickedOnEdit = function(lot, index){
				$scope.currentlot = angular.copy(lot);
				$scope.currentLotIndex = index;
				$scope.modalShownEdit = true;
			}

			$scope.saveDetailsEdited =function(){
				console.log($scope.currentlot);
			}

		};

	})();