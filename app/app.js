angular
	.module('app', [
		'ui.router',
		'ui.bootstrap',
		'ngAnimate',
		'angularSlideables',
		'ngStorage',
		'facebook',
		'google.places',
		'infinite-scroll',
		'ui.grid'
		// 'btford.socket-io'
		// 'slick',
		// 'rzModule',
		// 'btford.socket-io',
		// '720kb.socialshare',
	])
	.config(function(FacebookProvider) {
		FacebookProvider.init('278381285923341');
	})
	.directive("ngUploadChange",function(){
	    return{
	        scope:{
	            ngUploadChange:"&"
	        },
	        link:function($scope, $element, $attrs){
	            $element.on("change",function(event){
	                $scope.ngUploadChange({$event: event})
	            })
	            $scope.$on("$destroy",function(){
	                $element.off();
	            });
	        }
	    }
	}).directive('modalDialog', function() {
	  return {
	    restrict: 'E',
	    scope: {
	      show: '='
	    },
	    replace: true, // Replace with the template below
	    transclude: true, // we want to insert custom content inside the directive
	    link: function(scope, element, attrs) {
	      scope.dialogStyle = {};
	      if (attrs.width)
	        scope.dialogStyle.width = attrs.width;
	      if (attrs.height)
	        scope.dialogStyle.height = attrs.height;
	      scope.hideModal = function() {
	        scope.show = false;
	      };
	    },
	    template: "<div class='ng-modal' ng-show='show'>"+
		  "<div class='ng-modal-overlay' ng-click='hideModal()'></div>"+
		  "<div class='ng-modal-dialog' ng-style='dialogStyle'>"+
		    "<div class='ng-modal-close' ng-click='hideModal()'>X</div>"+
		    "<div class='ng-modal-dialog-content' ng-transclude></div>"+
		  "</div>"+
		"</div>" // See below
	  };
	});