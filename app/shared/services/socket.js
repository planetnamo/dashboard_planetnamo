(function() {
	'use strict';
	angular
	.module('app')
	.factory('socket', socket);


	function socket(socketFactory) {
		return socketFactory({
			ioSocket: io.connect('http://urbanriwaaz.com:8080')
		});
	};
})();