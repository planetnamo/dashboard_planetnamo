angular
	.module('app')
	.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state('login',{
				url: '/login',
				templateUrl: 'app/modules/login/login.html',
				controller: 'LoginController'
			})
			.state('dashboard',{
				abstract: true,
				url: '/dashboard',
				templateUrl: 'app/modules/dashboard/dashboard.html',
				controller: 'DashboardController'
			})
			.state('dashboard.auctions',{
				url: '/auctions',
				templateUrl: 'app/modules/dashboard/auctions/auctions.html',
				controller: 'AuctionsController'
			})
			.state('dashboard.clearance',{
				url: '/clearance',
				templateUrl: 'app/modules/dashboard/clearance/clearance.html',
				controller: 'ClearanceController'
			})
			.state('dashboard.buy',{
				url: '/buy',
				templateUrl: 'app/modules/dashboard/direct-buy/direct-buy.html',
				controller: 'DirectBuyController'
			})
			.state('dashboard.rentals',{
				url: '/rentals',
				templateUrl: 'app/modules/dashboard/rentals/rentals.html',
				controller: 'RentalsController'
			})
			.state('dashboard.profile',{
				url: '/profile',
				templateUrl: 'app/modules/dashboard/profile/profile.html',
				controller: 'ProfileController'
			})
			.state('dashboard.manage',{
				url: '/manage',
				templateUrl: 'app/modules/dashboard/manage/manage.html',
				controller: 'ManageController'
			})
			.state('rent', {
				abstract: true,
				url: '/rent',
				templateUrl: 'app/modules/rent/rent.html'
			})
			.state('sell', {
				url: '/sell',
				templateUrl: 'app/modules/sell/sell.html'
				// controller: 'CartPayController'
			})
			.state('sellProduct', {
				abstract: true,
				url: '/sell-product/:category',
				templateUrl: 'app/modules/sell/sell-product.html',
				controller: 'SellProductController'
			})
			.state('sellProduct.step1', {
				url: '/',
				templateUrl: 'app/modules/sell/step1/sell-product-step-1.html',
				controller: 'SellProductStep1Controller'
			})
			.state('sellProduct.step2', {
				url: '/step2',
				templateUrl: 'app/modules/sell/step2/sell-product-step-2.html',
				controller: 'SellProductStep2Controller'
			})
			.state('sellProduct.step3', {
				url: '/step3',
				templateUrl: 'app/modules/sell/step3/sell-product-step-3.html',
				controller: 'SellProductStep3Controller'
			})
			.state('sellProduct.step4', {
				url: '/step4',
				templateUrl: 'app/modules/sell/step4/sell-product-step-4.html',
				controller: 'SellProductStep4Controller'
			})
			.state('sellProduct.step5', {
				url: '/step5',
				templateUrl: 'app/modules/sell/step5/sell-product-step-5.html',
				controller: 'SellProductStep5Controller'
			})
			.state('sellProduct.step6', {
				url: '/step6',
				templateUrl: 'app/modules/sell/step6/sell-product-step-6.html',
				controller: 'SellProductStep6Controller'
			})
			.state('sellProduct.step7', {
				url: '/step7',
				templateUrl: 'app/modules/sell/step7/sell-product-step-7.html',
				controller: 'SellProductStep7Controller'
			})
			.state('sellProduct.step8', {
				url: '/step8',
				templateUrl: 'app/modules/sell/step8/sell-product-step-8.html',
				controller: 'SellProductStep8Controller'
			})
			.state('sellProduct.step9', {
				url: '/step9',
				templateUrl: 'app/modules/sell/step9/sell-product-step-9.html',
				controller: 'SellProductStep9Controller'
			})
			.state('dashboard.me', {
				abstract: true,
				url: '/me',
				templateUrl: 'app/modules/me/me.html',
				controller: 'MeController'
			})
			.state('dashboard.me.profile', {
				url: '/profile',
				templateUrl: 'app/modules/me/profile/profile.html',
				controller: 'MeProfileController'
			})
			.state('dashboard.me.wishlist', {
				url: '/wishlist',
				templateUrl: 'app/modules/me/wishlist/wishlist.html',
				controller: 'MeWishlistController'
			})
			.state('dashboard.me.wallet', {
				url: '/wallet',
				templateUrl: 'app/modules/me/wallet/wallet.html',
				controller: 'MeWalletController'
			})
			.state('dashboard.me.orders', {
				url: '/orders',
				templateUrl: 'app/modules/me/orders/orders.html',
				controller: 'MeOrderController'
			})
			.state('me.sales', {
				url: '/sales',
				templateUrl: 'app/modules/me/sales/sales.html',
				controller: 'MeSaleController'
			})
			.state('me.rentals', {
				url: '/rentals',
				templateUrl: 'app/modules/me/rentals/rentals.html',
				controller: 'MeRentalController'
			})
			.state('me.aggregate', {
				url: '/aggregate',
				templateUrl: 'app/modules/me/aggregate/aggregate.html',
				controller: 'MeAggregateController'
			})
			.state('me.auctionHall', {
				abstract: true,
				url: '/auction-hall',
				templateUrl: 'app/modules/me/auction-hall/auction-hall.html',
				controller: 'MeAuctionHallController'
			})
			.state('me.auctionHall.live', {
				url: '/live',
				templateUrl: 'app/modules/me/auction-hall/live/auction-hall-live.html',
				controller: 'MeAuctionHallLiveController'
			})
			.state('me.auctionHall.expired', {
				url: '/expired',
				templateUrl: 'app/modules/me/auction-hall/expired/auction-hall-expired.html',
				controller: 'MeAuctionHallExpiredController'
			})
			.state('me.auctionHall.won', {
				url: '/won',
				templateUrl: 'app/modules/me/auction-hall/won/auction-hall-won.html',
				controller: 'MeAuctionHallWonController'
			})
			.state('me.auctionHall.archive', {
				url: '/archive',
				templateUrl: 'app/modules/me/auction-hall/archive/auction-hall-archive.html',
				controller: 'MeAuctionHallArchiveController'
			});

		$urlRouterProvider.when('/dashboard', '/dashboard/auctions'); // Redirect in case user goes to the abstract state
		$urlRouterProvider.when('/rent', '/rent/products'); // Redirect in case user goes to the abstract state
		$urlRouterProvider.when('/sell-product', '/sell'); // Redirect in case user goes to the abstract state
		$urlRouterProvider.when('/me', '/me/profile'); // Redirect in case user goes to the abstract state
		$urlRouterProvider.when('/me/auction-hall', '/me/auction-hall/live'); // Redirect in case user goes to the abstract state
		$urlRouterProvider.otherwise('/dashboard');
	}]);