var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var minifyCSS = require('gulp-csso');

gulp.task('css', function() {
	gulp.src([
			'./assets/libs/bootstrap/css/bootstrap.min.css',
			'./assets/libs/font-awesome/css/font-awesome.min.css',
			'./assets/libs/slick/slick.css',
			'./assets/libs/slick/slick-theme.css',
			'./assets/libs/flipclock/flipclock.css',
			'./assets/libs/sweetalert/sweetalert.css',
			'./assets/libs/sweetalert/themes/google/google.css',
			'./assets/libs/angular-google-places-autocomplete/autocomplete.css',
			'./assets/libs/ui-bootstrap/ui-bootstrap-custom-2.5.0-csp.css',
			'./assets/libs/animatecss/animate.min.css',
			'./assets/css/style.css',
			'./assets/css/core/header.css',
			'./assets/css/core/footer.css',
			'./assets/css/core/modals.css',
			'./assets/css/signup/signup.css',
			'./assets/css/home/home.css',
			'./assets/css/home/hero-slider.css',
			'./assets/css/buy/buy.css',
			'./assets/css/rent/rent-products.css',
			'./assets/css/rent/rent-details.css',
			'./assets/css/details/direct-buy.css',
			'./assets/css/details/clearance.css',
			'./assets/css/details/auction.css',
			'./assets/css/cart/overview.css',
			'./assets/css/cart/delivery.css',
			'./assets/css/cart/pay.css',
			'./assets/css/sell/sell.css',
			'./assets/css/sell/sell-product.css',
			'./assets/css/sell/sell-product-step-1.css',
			'./assets/css/sell/sell-product-step-2.css',
			'./assets/css/sell/sell-product-step-3.css',
			'./assets/css/sell/sell-product-step-4.css',
			'./assets/css/sell/sell-product-step-5.css',
			'./assets/css/sell/sell-product-step-6.css',
			'./assets/css/sell/sell-product-step-7.css',
			'./assets/css/sell/sell-product-step-8.css',
			'./assets/css/sell/sell-product-step-9.css',
			'./assets/css/me/me.css',
			'./assets/css/me/me-profile.css',
			'./assets/css/me/me-wishlist.css',
			'./assets/css/me/me-wallet.css',
			'./assets/css/me/me-orders.css',
			'./assets/css/me/me-sales.css',
			'./assets/css/me/me-rentals.css',
			'./assets/css/me/me-aggregate.css',
			'./assets/css/me/auction-hall/me-auction-hall.css',
			'./assets/css/me/auction-hall/me-auction-hall-live.css',
			'./assets/css/me/auction-hall/me-auction-hall-expired.css',
			'./assets/css/me/auction-hall/me-auction-hall-won.css',
			'./assets/css/me/auction-hall/me-auction-hall-archive.css',
			'./assets/css/contact/contact.css',
			'./assets/css/about/about.css',
			'./assets/css/compare/compare.css'
		])
		.pipe(concat('style.css'))
		.pipe(minifyCSS())
		.pipe(gulp.dest('build'))
});

gulp.task('js', function() {
	// For libs
	gulp.src([
			'./assets/libs/jquery/jquery-2.2.3.min.js',
			'./assets/libs/bootstrap/js/bootstrap.js',
			'./assets/libs/slick/slick.js',
			'./assets/libs/moment/moment.js',
			'./assets/libs/countdown/jquery.countdown.js',
			'./assets/libs/angular/angular.js',
			'./assets/libs/angular/angular-ui-router-min.js',
			'./assets/libs/angular/angular-animate.min.js',
			'./assets/libs/angular/angularSlideables.js',
			'./assets/libs/angular/ngStorage.min.js',
			'./assets/libs/angular/angular-facebook.js',
			'./assets/libs/sweetalert/sweetalert.min.js',
			'./assets/libs/ui-bootstrap/ui-bootstrap-custom-2.5.0.js',
			'./assets/libs/ui-bootstrap/ui-bootstrap-custom-tpls-2.5.0.min.js',
			'./assets/libs/angular-google-places-autocomplete/autocomplete.js',
			'./assets/libs/flipclock/flipclock.min.js'
		])
		.pipe(concat('vendors.js'))
		.pipe(uglify().on('error', function(e){
            console.log(e);
         }))
		.pipe(gulp.dest('build'));

	gulp.src(['./build/vendors.js', './assets/libs/infiniteScroll/infinite_scroll.js'])
		.pipe(concat('vendors.js'))
		.pipe(gulp.dest('build'));

	// For app modules
	gulp.src([
			'./app/app.js',
			'./app/app.routes.js',
			'./app/shared/directives/core.directive.js',
			'./app/shared/filters/filters.js',
			'./app/shared/services/socket.js',
			'./app/modules/index/index.controller.js',
			'./app/modules/signup/signup.controller.js',
			'./app/modules/home/home.controller.js',
			'./app/modules/buy/buy.controller.js',
			'./app/modules/rent/products/rent-products.controller.js',
			'./app/modules/rent/details/rent-details.controller.js',
			'./app/modules/details/direct-buy/direct-buy.controller.js',
			'./app/modules/details/clearance/clearance.controller.js',
			'./app/modules/details/auction/auction.controller.js',
			'./app/modules/cart/delivery/delivery.controller.js',
			'./app/modules/cart/rental/rental.controller.js',
			'./app/modules/sell/sell-product.controller.js',
			'./app/modules/sell/step1/sell-product-step-1.controller.js',
			'./app/modules/sell/step2/sell-product-step-2.controller.js',
			'./app/modules/sell/step3/sell-product-step-3.controller.js',
			'./app/modules/sell/step4/sell-product-step-4.controller.js',
			'./app/modules/sell/step5/sell-product-step-5.controller.js',
			'./app/modules/sell/step6/sell-product-step-6.controller.js',
			'./app/modules/sell/step7/sell-product-step-7.controller.js',
			'./app/modules/sell/step8/sell-product-step-8.controller.js',
			'./app/modules/sell/step9/sell-product-step-9.controller.js',
			'./app/modules/me/me.controller.js',
			'./app/modules/me/profile/me-profile.controller.js',
			'./app/modules/me/wishlist/me-wishlist.controller.js',
			'./app/modules/me/wallet/me-wallet.controller.js',
			'./app/modules/me/orders/me-order.controller.js',
			'./app/modules/me/sales/me-sale.controller.js',
			'./app/modules/me/rentals/me-rental.controller.js',
			'./app/modules/me/aggregate/me-aggregate.controller.js',
			'./app/modules/me/auction-hall/me-auction-hall.controller.js',
			'./app/modules/me/auction-hall/live/me-auction-hall-live.controller.js',
			'./app/modules/me/auction-hall/expired/me-auction-hall-expired.controller.js',
			'./app/modules/me/auction-hall/won/me-auction-hall-won.controller.js',
			'./app/modules/me/auction-hall/archive/me-auction-hall-archive.controller.js',
			'./app/modules/contact/contact.controller.js',
			'./app/modules/about/about.controller.js',
			'./app/modules/compare/compare.controller.js',
			'./app/modules/compare/clearance/clearance.controller.js',
			'./app/modules/compare/auction/auction.controller.js',
			'./app/modules/compare/buy/buy.controller.js',
			'./app/modules/compare/rent/rent.controller.js',
			'./app/modules/network/networkFactory.js',
			'./app/modules/network/authFactory.js',
			'./app/modules/network/validationFactory.js'
		])
		.pipe(concat('scripts.js'))
		.pipe(uglify({ mangle: false }).on('error', function(e){
            console.log(e);
         }))
		.pipe(gulp.dest('build'));
});

gulp.task('default', ['css', 'js']);