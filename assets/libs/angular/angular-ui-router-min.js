"undefined"!=typeof module&&"undefined"!=typeof exports&&module.exports===exports&&(module.exports="ui.router"),function(t,e,r){"use strict"
function n(t,e){return U(new(U(function(){},{prototype:t})),e)}function a(t){return N(arguments,function(e){e!==t&&N(e,function(e,r){t.hasOwnProperty(r)||(t[r]=e)})}),t}function i(t,e){var r=[]
for(var n in t.path){if(t.path[n]!==e.path[n])break
r.push(t.path[n])}return r}function o(t){if(Object.keys)return Object.keys(t)
var e=[]
return N(t,function(t,r){e.push(r)}),e}function u(t,e){if(Array.prototype.indexOf)return t.indexOf(e,+arguments[2]||0)
var r=t.length>>>0,n=+arguments[2]||0
for(n=0>n?Math.ceil(n):Math.floor(n),0>n&&(n+=r);r>n;n++)if(n in t&&t[n]===e)return n
return-1}function s(t,e,r,n){var a,s=i(r,n),l={},c=[]
for(var f in s)if(s[f].params&&(a=o(s[f].params),a.length))for(var p in a)u(c,a[p])>=0||(c.push(a[p]),l[a[p]]=t[a[p]])
return U({},l,e)}function l(t,e,r){if(!r){r=[]
for(var n in t)r.push(n)}for(var a=0;a<r.length;a++){var i=r[a]
if(t[i]!=e[i])return!1}return!0}function c(t,e){var r={}
return N(t,function(t){r[t]=e[t]}),r}function f(t){var e={},r=Array.prototype.concat.apply(Array.prototype,Array.prototype.slice.call(arguments,1))
return N(r,function(r){r in t&&(e[r]=t[r])}),e}function p(t){var e={},r=Array.prototype.concat.apply(Array.prototype,Array.prototype.slice.call(arguments,1))
for(var n in t)-1==u(r,n)&&(e[n]=t[n])
return e}function h(t,e){var r=D(t),n=r?[]:{}
return N(t,function(t,a){e(t,a)&&(n[r?n.length:a]=t)}),n}function v(t,e){var r=D(t)?[]:{}
return N(t,function(t,n){r[n]=e(t,n)}),r}function $(t,e){var n=1,i=2,s={},l=[],c=s,f=U(t.when(s),{$$promises:s,$$values:s})
this.study=function(s){function h(t,r){if(g[r]!==i){if(m.push(r),g[r]===n)throw m.splice(0,u(m,r)),Error("Cyclic dependency: "+m.join(" -> "))
if(g[r]=n,R(t))d.push(r,[function(){return e.get(t)}],l)
else{var a=e.annotate(t)
N(a,function(t){t!==r&&s.hasOwnProperty(t)&&h(s[t],t)}),d.push(r,t,a)}m.pop(),g[r]=i}}function v(t){return F(t)&&t.then&&t.$$promises}if(!F(s))throw Error("'invocables' must be an object")
var $=o(s||{}),d=[],m=[],g={}
return N(s,h),s=m=g=null,function(n,i,o){function u(){--w||(b||a(y,i.$$values),m.$$values=y,m.$$promises=m.$$promises||!0,delete m.$$inheritedValues,h.resolve(y))}function s(t){m.$$failure=t,h.reject(t)}function l(r,a,i){function l(t){f.reject(t),s(t)}function c(){if(!V(m.$$failure))try{f.resolve(e.invoke(a,o,y)),f.promise.then(function(t){y[r]=t,u()},l)}catch(t){l(t)}}var f=t.defer(),p=0
N(i,function(t){g.hasOwnProperty(t)&&!n.hasOwnProperty(t)&&(p++,g[t].then(function(e){y[t]=e,--p||c()},l))}),p||c(),g[r]=f.promise}if(v(n)&&o===r&&(o=i,i=n,n=null),n){if(!F(n))throw Error("'locals' must be an object")}else n=c
if(i){if(!v(i))throw Error("'parent' must be a promise returned by $resolve.resolve()")}else i=f
var h=t.defer(),m=h.promise,g=m.$$promises={},y=U({},n),w=1+d.length/3,b=!1
if(V(i.$$failure))return s(i.$$failure),m
i.$$inheritedValues&&a(y,p(i.$$inheritedValues,$)),U(g,i.$$promises),i.$$values?(b=a(y,p(i.$$values,$)),m.$$inheritedValues=p(i.$$values,$),u()):(i.$$inheritedValues&&(m.$$inheritedValues=p(i.$$inheritedValues,$)),i.then(u,s))
for(var E=0,x=d.length;x>E;E+=3)n.hasOwnProperty(d[E])?u():l(d[E],d[E+1],d[E+2])
return m}},this.resolve=function(t,e,r,n){return this.study(t)(e,r,n)}}function d(t,e,r){this.fromConfig=function(t,e,r){return V(t.template)?this.fromString(t.template,e):V(t.templateUrl)?this.fromUrl(t.templateUrl,e):V(t.templateProvider)?this.fromProvider(t.templateProvider,e,r):null},this.fromString=function(t,e){return M(t)?t(e):t},this.fromUrl=function(r,n){return M(r)&&(r=r(n)),null==r?null:t.get(r,{cache:e,headers:{Accept:"text/html"}}).then(function(t){return t.data})},this.fromProvider=function(t,e,n){return r.invoke(t,null,n||{params:e})}}function m(t,e,a){function i(e,r,n,a){if(d.push(e),v[e])return v[e]
if(!/^\w+(-+\w+)*(?:\[\])?$/.test(e))throw Error("Invalid parameter name '"+e+"' in pattern '"+t+"'")
if($[e])throw Error("Duplicate parameter name '"+e+"' in pattern '"+t+"'")
return $[e]=new z.Param(e,r,n,a),$[e]}function o(t,e,r,n){var a=["",""],i=t.replace(/[\\\[\]\^$*+?.()|{}]/g,"\\$&")
if(!e)return i
switch(r){case!1:a=["(",")"+(n?"?":"")]
break
case!0:a=["?(",")?"]
break
default:a=["("+r+"|",")?"]}return i+a[0]+e+a[1]}function u(a,i){var o,u,s,l,c
return o=a[2]||a[3],c=e.params[o],s=t.substring(p,a.index),u=i?a[4]:a[4]||("*"==a[1]?".*":null),l=z.type(u||"string")||n(z.type("string"),{pattern:RegExp(u,e.caseInsensitive?"i":r)}),{id:o,regexp:u,segment:s,type:l,cfg:c}}e=U({params:{}},F(e)?e:{})
var s,l=/([:*])([\w\[\]]+)|\{([\w\[\]]+)(?:\:((?:[^{}\\]+|\\.|\{(?:[^{}\\]+|\\.)*\})+))?\}/g,c=/([:]?)([\w\[\]-]+)|\{([\w\[\]-]+)(?:\:((?:[^{}\\]+|\\.|\{(?:[^{}\\]+|\\.)*\})+))?\}/g,f="^",p=0,h=this.segments=[],v=a?a.params:{},$=this.params=a?a.params.$$new():new z.ParamSet,d=[]
this.source=t
for(var m,g,y;(s=l.exec(t))&&(m=u(s,!1),!(m.segment.indexOf("?")>=0));)g=i(m.id,m.type,m.cfg,"path"),f+=o(m.segment,g.type.pattern.source,g.squash,g.isOptional),h.push(m.segment),p=l.lastIndex
y=t.substring(p)
var w=y.indexOf("?")
if(w>=0){var b=this.sourceSearch=y.substring(w)
if(y=y.substring(0,w),this.sourcePath=t.substring(0,p+w),b.length>0)for(p=0;s=c.exec(b);)m=u(s,!0),g=i(m.id,m.type,m.cfg,"search"),p=l.lastIndex}else this.sourcePath=t,this.sourceSearch=""
f+=o(y)+(e.strict===!1?"/?":"")+"$",h.push(y),this.regexp=RegExp(f,e.caseInsensitive?"i":r),this.prefix=h[0],this.$$paramNames=d}function g(t){U(this,t)}function y(){function t(t){return null!=t?(""+t).replace(/\//g,"%2F"):t}function a(t){return null!=t?(""+t).replace(/%2F/g,"/"):t}function i(){return{strict:$,caseInsensitive:p}}function s(t){return M(t)||D(t)&&M(t[t.length-1])}function l(){for(;E.length;){var t=E.shift()
if(t.pattern)throw Error("You cannot override a type's .pattern at runtime.")
e.extend(w[t.name],f.invoke(t.def))}}function c(t){U(this,t||{})}z=this
var f,p=!1,$=!0,d=!1,w={},b=!0,E=[],x={string:{encode:t,decode:a,is:function(t){return null==t||!V(t)||"string"==typeof t},pattern:/[^\/]*/},"int":{encode:t,decode:function(t){return parseInt(t,10)},is:function(t){return V(t)&&this.decode(""+t)===t},pattern:/\d+/},bool:{encode:function(t){return t?1:0},decode:function(t){return 0!==parseInt(t,10)},is:function(t){return t===!0||t===!1},pattern:/0|1/},date:{encode:function(t){return this.is(t)?[t.getFullYear(),("0"+(t.getMonth()+1)).slice(-2),("0"+t.getDate()).slice(-2)].join("-"):r},decode:function(t){if(this.is(t))return t
var e=this.capture.exec(t)
return e?new Date(e[1],e[2]-1,e[3]):r},is:function(t){return t instanceof Date&&!isNaN(t.valueOf())},equals:function(t,e){return this.is(t)&&this.is(e)&&t.toISOString()===e.toISOString()},pattern:/[0-9]{4}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[1-2][0-9]|3[0-1])/,capture:/([0-9]{4})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])/},json:{encode:e.toJson,decode:e.fromJson,is:e.isObject,equals:e.equals,pattern:/[^\/]*/},any:{encode:e.identity,decode:e.identity,equals:e.equals,pattern:/.*/}}
y.$$getDefaultValue=function(t){if(!s(t.value))return t.value
if(!f)throw Error("Injectable functions cannot be called at configuration time")
return f.invoke(t.value)},this.caseInsensitive=function(t){return V(t)&&(p=t),p},this.strictMode=function(t){return V(t)&&($=t),$},this.defaultSquashPolicy=function(t){if(!V(t))return d
if(t!==!0&&t!==!1&&!R(t))throw Error("Invalid squash policy: "+t+". Valid policies: false, true, arbitrary-string")
return d=t,t},this.compile=function(t,e){return new m(t,U(i(),e))},this.isMatcher=function(t){if(!F(t))return!1
var e=!0
return N(m.prototype,function(r,n){M(r)&&(e=e&&V(t[n])&&M(t[n]))}),e},this.type=function(t,e,r){if(!V(e))return w[t]
if(w.hasOwnProperty(t))throw Error("A type named '"+t+"' has already been defined.")
return w[t]=new g(U({name:t},e)),r&&(E.push({name:t,def:r}),b||l()),this},N(x,function(t,e){w[e]=new g(U({name:e},t))}),w=n(w,{}),this.$get=["$injector",function(t){return f=t,b=!1,l(),N(x,function(t,e){w[e]||(w[e]=new g(t))}),this}],this.Param=function(t,e,n,a){function i(t){var e=F(t)?o(t):[],r=-1===u(e,"value")&&-1===u(e,"type")&&-1===u(e,"squash")&&-1===u(e,"array")
return r&&(t={value:t}),t.$$fn=s(t.value)?t.value:function(){return t.value},t}function l(e,r,n){if(e.type&&r)throw Error("Param '"+t+"' has two type configurations.")
return r?r:e.type?e.type instanceof g?e.type:new g(e.type):"config"===n?w.any:w.string}function c(){var e={array:"search"===a?"auto":!1},r=t.match(/\[\]$/)?{array:!0}:{}
return U(e,r,n).array}function p(t,e){var r=t.squash
if(!e||r===!1)return!1
if(!V(r)||null==r)return d
if(r===!0||R(r))return r
throw Error("Invalid squash policy: '"+r+"'. Valid policies: false, true, or arbitrary string")}function $(t,e,n,a){var i,o,s=[{from:"",to:n||e?r:""},{from:null,to:n||e?r:""}]
return i=D(t.replace)?t.replace:[],R(a)&&i.push({from:a,to:r}),o=v(i,function(t){return t.from}),h(s,function(t){return-1===u(o,t.from)}).concat(i)}function m(){if(!f)throw Error("Injectable functions cannot be called at configuration time")
var t=f.invoke(n.$$fn)
if(null!==t&&t!==r&&!E.type.is(t))throw Error("Default value ("+t+") for parameter '"+E.id+"' is not an instance of Type ("+E.type.name+")")
return t}function y(t){function e(t){return function(e){return e.from===t}}function r(t){var r=v(h(E.replace,e(t)),function(t){return t.to})
return r.length?r[0]:t}return t=r(t),V(t)?E.type.$normalize(t):m()}function b(){return"{Param:"+t+" "+e+" squash: '"+P+"' optional: "+S+"}"}var E=this
n=i(n),e=l(n,e,a)
var x=c()
e=x?e.$asArray(x,"search"===a):e,"string"!==e.name||x||"path"!==a||n.value!==r||(n.value="")
var S=n.value!==r,P=p(n,S),j=$(n,x,S,P)
U(this,{id:t,type:e,location:a,array:x,squash:P,replace:j,isOptional:S,value:y,dynamic:r,config:n,toString:b})},c.prototype={$$new:function(){return n(this,U(new c,{$$parent:this}))},$$keys:function(){for(var t=[],e=[],r=this,n=o(c.prototype);r;)e.push(r),r=r.$$parent
return e.reverse(),N(e,function(e){N(o(e),function(e){-1===u(t,e)&&-1===u(n,e)&&t.push(e)})}),t},$$values:function(t){var e={},r=this
return N(r.$$keys(),function(n){e[n]=r[n].value(t&&t[n])}),e},$$equals:function(t,e){var r=!0,n=this
return N(n.$$keys(),function(a){var i=t&&t[a],o=e&&e[a]
n[a].type.equals(i,o)||(r=!1)}),r},$$validates:function(t){var n,a,i,o,u,s=this.$$keys()
for(n=0;n<s.length&&(a=this[s[n]],i=t[s[n]],i!==r&&null!==i||!a.isOptional);n++){if(o=a.type.$normalize(i),!a.type.is(o))return!1
if(u=a.type.encode(o),e.isString(u)&&!a.type.pattern.exec(u))return!1}return!0},$$parent:r},this.ParamSet=c}function w(t,n){function a(t){var e=/^\^((?:\\[^a-zA-Z0-9]|[^\\\[\]\^$*+?.()|{}]+)*)/.exec(t.source)
return null!=e?e[1].replace(/\\(.)/g,"$1"):""}function i(t,e){return t.replace(/\$(\$|\d{1,2})/,function(t,r){return e["$"===r?0:+r]})}function o(t,e,r){if(!r)return!1
var n=t.invoke(e,e,{$match:r})
return V(n)?n:!0}function u(n,a,i,o){function u(t,e,r){return"/"===$?t:e?$.slice(0,-1)+t:r?$.slice(1)+t:t}function p(t){function e(t){var e=t(i,n)
return e?(R(e)&&n.replace().url(e),!0):!1}if(!t||!t.defaultPrevented){v&&n.url()===v
v=r
var a,o=l.length
for(a=0;o>a;a++)if(e(l[a]))return
c&&e(c)}}function h(){return s=s||a.$on("$locationChangeSuccess",p)}var v,$=o.baseHref(),d=n.url()
return f||h(),{sync:function(){p()},listen:function(){return h()},update:function(t){return t?(d=n.url(),r):(n.url()!==d&&(n.url(d),n.replace()),r)},push:function(t,e,a){var i=t.format(e||{})
null!==i&&e&&e["#"]&&(i+="#"+e["#"]),n.url(i),v=a&&a.$$avoidResync?n.url():r,a&&a.replace&&n.replace()},href:function(r,a,i){if(!r.validates(a))return null
var o=t.html5Mode()
e.isObject(o)&&(o=o.enabled)
var s=r.format(a)
if(i=i||{},o||null===s||(s="#"+t.hashPrefix()+s),null!==s&&a&&a["#"]&&(s+="#"+a["#"]),s=u(s,o,i.absolute),!i.absolute||!s)return s
var l=!o&&s?"/":"",c=n.port()
return c=80===c||443===c?"":":"+c,n.protocol()+"://"+n.host()+c+l+s}}}var s,l=[],c=null,f=!1
this.rule=function(t){if(!M(t))throw Error("'rule' must be a function")
return l.push(t),this},this.otherwise=function(t){if(R(t)){var e=t
t=function(){return e}}else if(!M(t))throw Error("'rule' must be a function")
return c=t,this},this.when=function(t,e){var r,u=R(e)
if(R(t)&&(t=n.compile(t)),!u&&!M(e)&&!D(e))throw Error("invalid 'handler' in when()")
var s={matcher:function(t,e){return u&&(r=n.compile(e),e=["$match",function(t){return r.format(t)}]),U(function(r,n){return o(r,e,t.exec(n.path(),n.search()))},{prefix:R(t.prefix)?t.prefix:""})},regex:function(t,e){if(t.global||t.sticky)throw Error("when() RegExp must not be global or sticky")
return u&&(r=e,e=["$match",function(t){return i(r,t)}]),U(function(r,n){return o(r,e,t.exec(n.path()))},{prefix:a(t)})}},l={matcher:n.isMatcher(t),regex:t instanceof RegExp}
for(var c in l)if(l[c])return this.rule(s[c](t,e))
throw Error("invalid 'what' in when()")},this.deferIntercept=function(t){t===r&&(t=!0),f=t},this.$get=u,u.$inject=["$location","$rootScope","$injector","$browser"]}function b(t,a){function i(t){return 0===t.indexOf(".")||0===t.indexOf("^")}function p(t,e){if(!t)return r
var n=R(t),a=n?t:t.name,o=i(a)
if(o){if(!e)throw Error("No reference point given for path '"+a+"'")
e=p(e)
for(var u=a.split("."),s=0,l=u.length,c=e;l>s;s++)if(""!==u[s]||0!==s){if("^"!==u[s])break
if(!c.parent)throw Error("Path '"+a+"' not valid for state '"+e.name+"'")
c=c.parent}else c=e
u=u.slice(s).join("."),a=c.name+(c.name&&u?".":"")+u}var f=P[a]
return!f||!n&&(n||f!==t&&f.self!==t)?r:f}function h(t,e){j[t]||(j[t]=[]),j[t].push(e)}function $(t){for(var e=j[t]||[];e.length;)d(e.shift())}function d(e){e=n(e,{self:e,resolve:e.resolve||{},toString:function(){return this.name}})
var r=e.name
if(!R(r)||r.indexOf("@")>=0)throw Error("State must have a valid name")
if(P.hasOwnProperty(r))throw Error("State '"+r+"'' is already defined")
var a=-1!==r.indexOf(".")?r.substring(0,r.lastIndexOf(".")):R(e.parent)?e.parent:F(e.parent)&&R(e.parent.name)?e.parent.name:""
if(a&&!P[a])return h(a,e.self)
for(var i in O)M(O[i])&&(e[i]=O[i](e,O.$delegates[i]))
return P[r]=e,!e[A]&&e.url&&t.when(e.url,["$match","$stateParams",function(t,r){S.$current.navigable==e&&l(t,r)||S.transitionTo(e,t,{inherit:!0,location:!1})}]),$(r),e}function m(t){return t.indexOf("*")>-1}function g(t){for(var e=t.split("."),r=S.$current.name.split("."),n=0,a=e.length;a>n;n++)"*"===e[n]&&(r[n]="*")
return"**"===e[0]&&(r=r.slice(u(r,e[1])),r.unshift("**")),"**"===e[e.length-1]&&(r.splice(u(r,e[e.length-2])+1,Number.MAX_VALUE),r.push("**")),e.length!=r.length?!1:r.join("")===e.join("")}function y(t,e){return R(t)&&!V(e)?O[t]:M(e)&&R(t)?(O[t]&&!O.$delegates[t]&&(O.$delegates[t]=O[t]),O[t]=e,this):this}function w(t,e){return F(t)?e=t:e.name=t,d(e),this}function b(t,a,i,u,f,h,$,d,y){function w(e,r,n,i){var o=t.$broadcast("$stateNotFound",e,r,n)
if(o.defaultPrevented)return $.update(),C
if(!o.retry)return null
if(i.$retry)return $.update(),q
var u=S.transition=a.when(o.retry)
return u.then(function(){return u!==S.transition?j:(e.options.$retry=!0,S.transitionTo(e.to,e.toParams,e.options))},function(){return C}),$.update(),u}function b(t,r,n,o,s,l){function p(){var r=[]
return N(t.views,function(n,a){var o=n.resolve&&n.resolve!==t.resolve?n.resolve:{}
o.$template=[function(){return i.load(a,{view:n,locals:s.globals,params:h,notify:l.notify})||""}],r.push(f.resolve(o,s.globals,s.resolve,t).then(function(r){if(M(n.controllerProvider)||D(n.controllerProvider)){var i=e.extend({},o,s.globals)
r.$$controller=u.invoke(n.controllerProvider,null,i)}else r.$$controller=n.controller
r.$$state=t,r.$$controllerAs=n.controllerAs,s[a]=r}))}),a.all(r).then(function(){return s.globals})}var h=n?r:c(t.params.$$keys(),r),v={$stateParams:h}
s.resolve=f.resolve(t.resolve,v,s.resolve,t)
var $=[s.resolve.then(function(t){s.globals=t})]
return o&&$.push(o),a.all($).then(p).then(function(t){return s})}var j=a.reject(Error("transition superseded")),O=a.reject(Error("transition prevented")),C=a.reject(Error("transition aborted")),q=a.reject(Error("transition failed"))
return x.locals={resolve:null,globals:{$stateParams:{}}},S={params:{},current:x.self,$current:x,transition:null},S.reload=function(t){return S.transitionTo(S.current,h,{reload:t||!0,inherit:!1,notify:!0})},S.go=function(t,e,r){return S.transitionTo(t,e,U({inherit:!0,relative:S.$current},r))},S.transitionTo=function(e,r,i){r=r||{},i=U({location:!0,inherit:!1,relative:null,notify:!0,reload:!1,$retry:!1},i||{})
var o,l=S.$current,f=S.params,v=l.path,d=p(e,i.relative),m=r["#"]
if(!V(d)){var g={to:e,toParams:r,options:i},y=w(g,l.self,f,i)
if(y)return y
if(e=g.to,r=g.toParams,i=g.options,d=p(e,i.relative),!V(d)){if(!i.relative)throw Error("No such state '"+e+"'")
throw Error("Could not resolve '"+e+"' from state '"+i.relative+"'")}}if(d[A])throw Error("Cannot transition to abstract state '"+e+"'")
if(i.inherit&&(r=s(h,r||{},S.$current,d)),!d.params.$$validates(r))return q
r=d.params.$$values(r),e=d
var P=e.path,C=0,k=P[C],I=x.locals,M=[]
if(i.reload){if(R(i.reload)||F(i.reload)){if(F(i.reload)&&!i.reload.name)throw Error("Invalid reload state object")
var D=i.reload===!0?v[0]:p(i.reload)
if(i.reload&&!D)throw Error("No such reload state '"+(R(i.reload)?i.reload:i.reload.name)+"'")
for(;k&&k===v[C]&&k!==D;)I=M[C]=k.locals,C++,k=P[C]}}else for(;k&&k===v[C]&&k.ownParams.$$equals(r,f);)I=M[C]=k.locals,C++,k=P[C]
if(E(e,r,l,f,I,i))return m&&(r["#"]=m),S.params=r,T(S.params,h),i.location&&e.navigable&&e.navigable.url&&($.push(e.navigable.url,r,{$$avoidResync:!0,replace:"replace"===i.location}),$.update(!0)),S.transition=null,a.when(S.current)
if(r=c(e.params.$$keys(),r||{}),i.notify&&t.$broadcast("$stateChangeStart",e.self,r,l.self,f).defaultPrevented)return t.$broadcast("$stateChangeCancel",e.self,r,l.self,f),$.update(),O
for(var N=a.when(I),z=C;z<P.length;z++,k=P[z])I=M[z]=n(I),N=b(k,r,k===e,N,I,i)
var L=S.transition=N.then(function(){var n,a,o
if(S.transition!==L)return j
for(n=v.length-1;n>=C;n--)o=v[n],o.self.onExit&&u.invoke(o.self.onExit,o.self,o.locals.globals),o.locals=null
for(n=C;n<P.length;n++)a=P[n],a.locals=M[n],a.self.onEnter&&u.invoke(a.self.onEnter,a.self,a.locals.globals)
return m&&(r["#"]=m),S.transition!==L?j:(S.$current=e,S.current=e.self,S.params=r,T(S.params,h),S.transition=null,i.location&&e.navigable&&$.push(e.navigable.url,e.navigable.locals.globals.$stateParams,{$$avoidResync:!0,replace:"replace"===i.location}),i.notify&&t.$broadcast("$stateChangeSuccess",e.self,r,l.self,f),$.update(!0),S.current)},function(n){return S.transition!==L?j:(S.transition=null,o=t.$broadcast("$stateChangeError",e.self,r,l.self,f,n),o.defaultPrevented||$.update(),a.reject(n))})
return L},S.is=function(t,e,n){n=U({relative:S.$current},n||{})
var a=p(t,n.relative)
return V(a)?S.$current!==a?!1:e?l(a.params.$$values(e),h):!0:r},S.includes=function(t,e,n){if(n=U({relative:S.$current},n||{}),R(t)&&m(t)){if(!g(t))return!1
t=S.$current.name}var a=p(t,n.relative)
return V(a)?V(S.$current.includes[a.name])?e?l(a.params.$$values(e),h,o(e)):!0:!1:r},S.href=function(t,e,n){n=U({lossy:!0,inherit:!0,absolute:!1,relative:S.$current},n||{})
var a=p(t,n.relative)
if(!V(a))return null
n.inherit&&(e=s(h,e||{},S.$current,a))
var i=a&&n.lossy?a.navigable:a
return i&&i.url!==r&&null!==i.url?$.href(i.url,c(a.params.$$keys().concat("#"),e||{}),{absolute:n.absolute}):null},S.get=function(t,e){if(0===arguments.length)return v(o(P),function(t){return P[t].self})
var r=p(t,e||S.$current)
return r&&r.self?r.self:null},S}function E(t,e,n,a,i,o){function u(t,e,r){function n(e){return"search"!=t.params[e].location}var a=t.params.$$keys().filter(n),i=f.apply({},[t.params].concat(a)),o=new z.ParamSet(i)
return o.$$equals(e,r)}return!o.reload&&t===n&&(i===n.locals||t.self.reloadOnSearch===!1&&u(n,a,e))?!0:r}var x,S,P={},j={},A="abstract",O={parent:function(t){if(V(t.parent)&&t.parent)return p(t.parent)
var e=/^(.+)\.[^.]+$/.exec(t.name)
return e?p(e[1]):x},data:function(t){return t.parent&&t.parent.data&&(t.data=t.self.data=U({},t.parent.data,t.data)),t.data},url:function(t){var e=t.url,r={params:t.params||{}}
if(R(e))return"^"==e.charAt(0)?a.compile(e.substring(1),r):(t.parent.navigable||x).url.concat(e,r)
if(!e||a.isMatcher(e))return e
throw Error("Invalid url '"+e+"' in state '"+t+"'")},navigable:function(t){return t.url?t:t.parent?t.parent.navigable:null},ownParams:function(t){var e=t.url&&t.url.params||new z.ParamSet
return N(t.params||{},function(t,r){e[r]||(e[r]=new z.Param(r,null,t,"config"))}),e},params:function(t){return t.parent&&t.parent.params?U(t.parent.params.$$new(),t.ownParams):new z.ParamSet},views:function(t){var e={}
return N(V(t.views)?t.views:{"":t},function(r,n){n.indexOf("@")<0&&(n+="@"+t.parent.name),e[n]=r}),e},path:function(t){return t.parent?t.parent.path.concat(t):[]},includes:function(t){var e=t.parent?U({},t.parent.includes):{}
return e[t.name]=!0,e},$delegates:{}}
x=d({name:"",url:"^",views:null,"abstract":!0}),x.navigable=null,this.decorator=y,this.state=w,this.$get=b,b.$inject=["$rootScope","$q","$view","$injector","$resolve","$stateParams","$urlRouter","$location","$urlMatcherFactory"]}function E(){function t(t,e){return{load:function(r,n){var a,i={template:null,controller:null,view:null,locals:null,notify:!0,async:!0,params:{}}
return n=U(i,n),n.view&&(a=e.fromConfig(n.view,n.params,n.locals)),a&&n.notify&&t.$broadcast("$viewContentLoading",n),a}}}this.$get=t,t.$inject=["$rootScope","$templateFactory"]}function x(){var t=!1
this.useAnchorScroll=function(){t=!0},this.$get=["$anchorScroll","$timeout",function(e,r){return t?e:function(t){return r(function(){t[0].scrollIntoView()},0,!1)}}]}function S(t,r,n,a){function i(){return r.has?function(t){return r.has(t)?r.get(t):null}:function(t){try{return r.get(t)}catch(e){return null}}}function o(t,e){var r=function(){return{enter:function(t,e,r){e.after(t),r()},leave:function(t,e){t.remove(),e()}}}
if(l)return{enter:function(t,e,r){var n=l.enter(t,null,e,r)
n&&n.then&&n.then(r)},leave:function(t,e){var r=l.leave(t,e)
r&&r.then&&r.then(e)}}
if(s){var n=s&&s(e,t)
return{enter:function(t,e,r){n.enter(t,null,e),r()},leave:function(t,e){n.leave(t),e()}}}return r()}var u=i(),s=u("$animator"),l=u("$animate"),c={restrict:"ECA",terminal:!0,priority:400,transclude:"element",compile:function(r,i,u){return function(r,i,s){function l(){f&&(f.remove(),f=null),h&&(h.$destroy(),h=null),p&&(m.leave(p,function(){f=null}),f=p,p=null)}function c(o){var c,f=j(r,s,i,a),g=f&&t.$current&&t.$current.locals[f]
if(o||g!==v){c=r.$new(),v=t.$current.locals[f]
var y=u(c,function(t){m.enter(t,i,function(){h&&h.$emit("$viewContentAnimationEnded"),(e.isDefined(d)&&!d||r.$eval(d))&&n(t)}),l()})
p=y,h=c,h.$emit("$viewContentLoaded"),h.$eval($)}}var f,p,h,v,$=s.onload||"",d=s.autoscroll,m=o(s,r)
r.$on("$stateChangeSuccess",function(){c(!1)}),r.$on("$viewContentLoading",function(){c(!1)}),c(!0)}}}
return c}function P(t,e,r,n){return{restrict:"ECA",priority:-400,compile:function(a){var i=a.html()
return function(a,o,u){var s=r.$current,l=j(a,u,o,n),c=s&&s.locals[l]
if(c){o.data("$uiView",{name:l,state:c.$$state}),o.html(c.$template?c.$template:i)
var f=t(o.contents())
if(c.$$controller){c.$scope=a,c.$element=o
var p=e(c.$$controller,c)
c.$$controllerAs&&(a[c.$$controllerAs]=p),o.data("$ngControllerController",p),o.children().data("$ngControllerController",p)}f(a)}}}}}function j(t,e,r,n){var a=n(e.uiView||e.name||"")(t),i=r.inheritedData("$uiView")
return a.indexOf("@")>=0?a:a+"@"+(i?i.state.name:"")}function A(t,e){var r,n=t.match(/^\s*({[^}]*})\s*$/)
if(n&&(t=e+"("+n[1]+")"),r=t.replace(/\n/g," ").match(/^([^(]+?)\s*(\((.*)\))?$/),!r||4!==r.length)throw Error("Invalid state ref '"+t+"'")
return{state:r[1],paramExpr:r[3]||null}}function O(t){var e=t.parent().inheritedData("$uiView")
return e&&e.state&&e.state.name?e.state:r}function C(t,n){var a=["location","inherit","reload","absolute"]
return{restrict:"A",require:["?^uiSrefActive","?^uiSrefActiveEq"],link:function(i,o,u,s){var l=A(u.uiSref,t.current.name),c=null,f=O(o)||t.$current,p="[object SVGAnimatedString]"===Object.prototype.toString.call(o.prop("href"))?"xlink:href":"href",h=null,v="A"===o.prop("tagName").toUpperCase(),$="FORM"===o[0].nodeName,d=$?"action":p,m=!0,g={relative:f,inherit:!0},y=i.$eval(u.uiSrefOpts)||{}
e.forEach(a,function(t){t in y&&(g[t]=y[t])})
var w=function(n){if(n&&(c=e.copy(n)),m){h=t.href(l.state,c,g)
var a=s[1]||s[0]
return a&&a.$$addStateInfo(l.state,c),null===h?(m=!1,!1):(u.$set(d,h),r)}}
l.paramExpr&&(i.$watch(l.paramExpr,function(t,e){t!==c&&w(t)},!0),c=e.copy(i.$eval(l.paramExpr))),w(),$||o.bind("click",function(e){var r=e.which||e.button
if(!(r>1||e.ctrlKey||e.metaKey||e.shiftKey||o.attr("target"))){var a=n(function(){t.go(l.state,c,g)})
e.preventDefault()
var i=v&&!h?1:0
e.preventDefault=function(){i--<=0&&n.cancel(a)}}})}}}function q(t,e,n){return{restrict:"A",controller:["$scope","$element","$attrs",function(e,a,i){function o(){u()?a.addClass(l):a.removeClass(l)}function u(){for(var t=0;t<c.length;t++)if(s(c[t].state,c[t].params))return!0
return!1}function s(e,n){return r!==i.uiSrefActiveEq?t.is(e.name,n):t.includes(e.name,n)}var l,c=[]
l=n(i.uiSrefActiveEq||i.uiSrefActive||"",!1)(e),this.$$addStateInfo=function(e,r){var n=t.get(e,O(a))
c.push({state:n||{name:e},params:r}),o()},e.$on("$stateChangeSuccess",o)}]}}function k(t){var e=function(e){return t.is(e)}
return e.$stateful=!0,e}function I(t){var e=function(e){return t.includes(e)}
return e.$stateful=!0,e}var V=e.isDefined,M=e.isFunction,R=e.isString,F=e.isObject,D=e.isArray,N=e.forEach,U=e.extend,T=e.copy
e.module("ui.router.util",["ng"]),e.module("ui.router.router",["ui.router.util"]),e.module("ui.router.state",["ui.router.router","ui.router.util"]),e.module("ui.router",["ui.router.state"]),e.module("ui.router.compat",["ui.router"]),$.$inject=["$q","$injector"],e.module("ui.router.util").service("$resolve",$),d.$inject=["$http","$templateCache","$injector"],e.module("ui.router.util").service("$templateFactory",d)
var z
m.prototype.concat=function(t,e){var r={caseInsensitive:z.caseInsensitive(),strict:z.strictMode(),squash:z.defaultSquashPolicy()}
return new m(this.sourcePath+t+this.sourceSearch,U(r,e),this)},m.prototype.toString=function(){return this.source},m.prototype.exec=function(t,e){function r(t){function e(t){return t.split("").reverse().join("")}function r(t){return t.replace(/\\-/g,"-")}var n=e(t).split(/-(?!\\)/),a=v(n,e)
return v(a,r).reverse()}var n=this.regexp.exec(t)
if(!n)return null
e=e||{}
var a,i,o,u=this.parameters(),s=u.length,l=this.segments.length-1,c={}
if(l!==n.length-1)throw Error("Unbalanced capture group in route '"+this.source+"'")
for(a=0;l>a;a++){o=u[a]
var f=this.params[o],p=n[a+1]
for(i=0;i<f.replace;i++)f.replace[i].from===p&&(p=f.replace[i].to)
p&&f.array===!0&&(p=r(p)),c[o]=f.value(p)}for(;s>a;a++)o=u[a],c[o]=this.params[o].value(e[o])
return c},m.prototype.parameters=function(t){return V(t)?this.params[t]||null:this.$$paramNames},m.prototype.validates=function(t){return this.params.$$validates(t)},m.prototype.format=function(t){function e(t){return encodeURIComponent(t).replace(/-/g,function(t){return"%5C%"+t.charCodeAt(0).toString(16).toUpperCase()})}t=t||{}
var r=this.segments,n=this.parameters(),a=this.params
if(!this.validates(t))return null
var i,o=!1,u=r.length-1,s=n.length,l=r[0]
for(i=0;s>i;i++){var c=u>i,f=n[i],p=a[f],h=p.value(t[f]),$=p.isOptional&&p.type.equals(p.value(),h),d=$?p.squash:!1,m=p.type.encode(h)
if(c){var g=r[i+1]
if(d===!1)null!=m&&(l+=D(m)?v(m,e).join("-"):encodeURIComponent(m)),l+=g
else if(d===!0){var y=l.match(/\/$/)?/\/?(.*)/:/(.*)/
l+=g.match(y)[1]}else R(d)&&(l+=d+g)}else{if(null==m||$&&d!==!1)continue
D(m)||(m=[m]),m=v(m,encodeURIComponent).join("&"+f+"="),l+=(o?"&":"?")+(f+"="+m),o=!0}}return l},g.prototype.is=function(t,e){return!0},g.prototype.encode=function(t,e){return t},g.prototype.decode=function(t,e){return t},g.prototype.equals=function(t,e){return t==e},g.prototype.$subPattern=function(){var t=""+this.pattern
return t.substr(1,t.length-2)},g.prototype.pattern=/.*/,g.prototype.toString=function(){return"{Type:"+this.name+"}"},g.prototype.$normalize=function(t){return this.is(t)?t:this.decode(t)},g.prototype.$asArray=function(t,e){function n(t,e){function n(t,e){return function(){return t[e].apply(t,arguments)}}function a(t){return D(t)?t:V(t)?[t]:[]}function i(t){switch(t.length){case 0:return r
case 1:return"auto"===e?t[0]:t
default:return t}}function o(t){return!t}function u(t,e){return function(r){r=a(r)
var n=v(r,t)
return e===!0?0===h(n,o).length:i(n)}}function s(t){return function(e,r){var n=a(e),i=a(r)
if(n.length!==i.length)return!1
for(var o=0;o<n.length;o++)if(!t(n[o],i[o]))return!1
return!0}}this.encode=u(n(t,"encode")),this.decode=u(n(t,"decode")),this.is=u(n(t,"is"),!0),this.equals=s(n(t,"equals")),this.pattern=t.pattern,this.$normalize=u(n(t,"$normalize")),this.name=t.name,this.$arrayMode=e}if(!t)return this
if("auto"===t&&!e)throw Error("'auto' array mode is for query parameters only")
return new n(this,t)},e.module("ui.router.util").provider("$urlMatcherFactory",y),e.module("ui.router.util").run(["$urlMatcherFactory",function(t){}]),w.$inject=["$locationProvider","$urlMatcherFactoryProvider"],e.module("ui.router.router").provider("$urlRouter",w),b.$inject=["$urlRouterProvider","$urlMatcherFactoryProvider"],e.module("ui.router.state").value("$stateParams",{}).provider("$state",b),E.$inject=[],e.module("ui.router.state").provider("$view",E),e.module("ui.router.state").provider("$uiViewScroll",x),S.$inject=["$state","$injector","$uiViewScroll","$interpolate"],P.$inject=["$compile","$controller","$state","$interpolate"],e.module("ui.router.state").directive("uiView",S),e.module("ui.router.state").directive("uiView",P),C.$inject=["$state","$timeout"],q.$inject=["$state","$stateParams","$interpolate"],e.module("ui.router.state").directive("uiSref",C).directive("uiSrefActive",q).directive("uiSrefActiveEq",q),k.$inject=["$state"],I.$inject=["$state"],e.module("ui.router.state").filter("isState",k).filter("includedByState",I)}(window,window.angular)
